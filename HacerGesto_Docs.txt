Behavioural Notes:
All single finger gestures cancel themselves when a second finger goes down this is the same for the Rotate gesture when dialMode is set to true.
Though these gestures cancel themselves the touch is still reflected in the HGResult.
All two finger gestures cancel themselves when the second finger goes up meaning both fingers would need to be released to rework the gesture.

When move and scale: Used together; over scaling can crash the library. Also if Scale or Rotate is in snap to centre mode the cantering will be thrown out. This doesn't seem to be the case when both have the set to centre flag set. This is not a problem because using only using these two gestures would make cantering obsolete.

When using Move/Rotate: If cumulative move is set to false the image will move slightly off centre of the touch. If Move is set to Dial mode the move will become disabled. This is not a problem as when in dial mode they are both single finger gestures.

When using Move/Scale: If snap to centre is enabled and cumulative move is disabled the image will drag off centre. The distance off centre will increase when scaling up.

Move/Scale/Rotate Using these gestures together tend to knock the angle reflected in the HGResult object. An workaround is to dynamically set the snap to centre on the rotate gesture.