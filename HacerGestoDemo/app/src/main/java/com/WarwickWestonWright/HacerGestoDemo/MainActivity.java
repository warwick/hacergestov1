package com.WarwickWestonWright.HacerGestoDemo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import warwickwestonwright.hacergestodemo.R;

public class MainActivity extends AppCompatActivity {

    private MainFragment mainFragment;
    private RotateFragment rotateFragment;
    private ScaleFragment scaleFragment;
    private MoveFragment moveFragment;
    private FlingFragment flingFragment;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        if(sp.getBoolean("spIsSetup", false) == false) {

            //Initialise Rotate settings.
            //Rotate Values
            sp.edit().putBoolean("RotateEnabled", false).commit();
            sp.edit().putBoolean("RotateSnapToCentre", false).commit();
            sp.edit().putBoolean("RotateDialMode", false).commit();
            sp.edit().putBoolean("RotateSnapMode", false).commit();
            sp.edit().putFloat("SnapToAngle", 0).commit();
            sp.edit().putFloat("AngleSnapTolerance", 0).commit();

            //Scale Values
            sp.edit().putBoolean("ScaleEnabled", false).commit();
            sp.edit().putBoolean("ScaleSnapToCentre", false).commit();
            sp.edit().putBoolean("ScaleSnappingEnabled", false).commit();
            sp.edit().putFloat("MinimumSnap", 0).commit();
            sp.edit().putFloat("MaximumSnap", 0).commit();
            sp.edit().putFloat("ScaleThreashold", 0).commit();

            //Move Values
            sp.edit().putBoolean("MoveEnabled", false).commit();
            sp.edit().putBoolean("CumulativeMove", false).commit();
            sp.edit().putBoolean("MoveSnapping", false).commit();
            sp.edit().putFloat("MoveSnappingPositionX", 0).commit();
            sp.edit().putFloat("MoveSnappingPositionY", 0).commit();
            sp.edit().putFloat("MoveSnappingTolerance", 0).commit();

            //Fling Values
            sp.edit().putBoolean("FlingEnabled", false).commit();
            sp.edit().putBoolean("BounceBack", false).commit();
            sp.edit().putInt("FlingDistance", 0).commit();
            sp.edit().putInt("FlingDistanceThreashold", 0).commit();
            sp.edit().putInt("FlingTimeThreashold", 0).commit();
            sp.edit().putInt("FlingAnimationTime", 0).commit();

            sp.edit().putBoolean("spIsSetup", true).commit();

        }//End if(sp.getBoolean("spIsSetup", false) == false)

        if(savedInstanceState == null) {

            scaleFragment = new ScaleFragment();
            rotateFragment = new RotateFragment();
            mainFragment = new MainFragment();
            moveFragment = new MoveFragment();
            flingFragment = new FlingFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainFragment).commit();

        }//End if(savedInstanceState == null)

    }//End protected void onCreate(Bundle savedInstanceState)


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.Rotate) {

            rotateFragment = new RotateFragment();
            rotateFragment.setCancelable(false);
            rotateFragment.setTargetFragment(mainFragment, 0);
            rotateFragment.show(getSupportFragmentManager(), "RotateFragment");

            return true;

        }
        else if(id == R.id.Scale) {

            scaleFragment = new ScaleFragment();
            scaleFragment.setCancelable(false);
            scaleFragment.setTargetFragment(mainFragment, 1);
            scaleFragment.show(getSupportFragmentManager(), "ScaleFragment");

            return true;

        }
        else if(id == R.id.Move) {

            moveFragment = new MoveFragment();
            moveFragment.setCancelable(false);
            moveFragment.setTargetFragment(mainFragment, 2);
            moveFragment.show(getSupportFragmentManager(), "MoveFragment");

            return true;

        }
        else if(id == R.id.Fling) {

            flingFragment = new FlingFragment();
            flingFragment.setCancelable(false);
            flingFragment.setTargetFragment(mainFragment, 3);
            flingFragment.show(getSupportFragmentManager(), "FlingFragment");

            return true;

        }

        return super.onOptionsItemSelected(item);

    }//End public boolean onOptionsItemSelected(MenuItem item)

}