package com.WarwickWestonWright.HacerGestoDemo;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import warwickwestonwright.hacergestodemo.R;

public class RotateFragment extends DialogFragment {

    private DialogInterface.OnClickListener dialogClickListener;

    private SharedPreferences sp;

    //Declare Widgets
    private Button btnCloseAndRotate;
    private CheckBox chkEnable;
    private CheckBox chkSnapToCentre;
    private CheckBox chkDialMode;
    private CheckBox chkSnapMode;
    private EditText txtEnterThreashold;
    private View rootView;

    private float[] angleThreasholdVals;

    public RotateFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        angleThreasholdVals = new float[2];

        try {

            dialogClickListener = (DialogInterface.OnClickListener) getTargetFragment();

        }
        catch (ClassCastException e) {

            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");

        }

    }//End public void onCreate(Bundle savedInstanceState)


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.rotate_fragment, container, false);

        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //Instantiate Widgets
        btnCloseAndRotate = (Button) rootView.findViewById(R.id.btnCloseAndRotate);
        chkEnable = (CheckBox) rootView.findViewById(R.id.chkEnable);
        chkSnapToCentre = (CheckBox) rootView.findViewById(R.id.chkSnapToCentre);
        chkDialMode = (CheckBox) rootView.findViewById(R.id.chkDialMode);
        chkSnapMode = (CheckBox) rootView.findViewById(R.id.chkSnapMode);
        txtEnterThreashold = (EditText) rootView.findViewById(R.id.txtEnterThreashold);

        String tempThreashold = Integer.toString(((int) sp.getFloat("SnapToAngle", 0))) + ", " +
                Float.toString(sp.getFloat("AngleSnapTolerance", 0));

        if(tempThreashold.equals("0, 0.0") == true) {

            txtEnterThreashold.setText("45, 0.5");

        }
        else {

            txtEnterThreashold.setText(tempThreashold);

        }//End if(tempThreashold.equals("0, 0.0") == true)

        //Initialise Widgets
        chkEnable.setChecked(sp.getBoolean("RotateEnabled", false));
        chkSnapToCentre.setChecked(sp.getBoolean("RotateSnapToCentre", false));
        chkDialMode.setChecked(sp.getBoolean("RotateDialMode", false));
        chkSnapMode.setChecked(sp.getBoolean("RotateSnapMode", false));

        chkEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chkEnable.isChecked() == true) {

                    sp.edit().putBoolean("RotateEnabled", true).commit();

                }
                else {

                    sp.edit().putBoolean("RotateEnabled", false).commit();

                }//End if(chkEnable.isChecked() == true)

            }

        });


        chkSnapToCentre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chkSnapToCentre.isChecked() == true) {

                    sp.edit().putBoolean("RotateSnapToCentre", true).commit();

                }
                else {

                    sp.edit().putBoolean("RotateSnapToCentre", false).commit();

                }//End if(chkSnapToCentre.isChecked() == true)

            }

        });


        chkDialMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chkDialMode.isChecked() == true) {

                    sp.edit().putBoolean("RotateDialMode", true).commit();

                }
                else {

                    sp.edit().putBoolean("RotateDialMode", false).commit();

                }//End if(chkSnapToCentre.isChecked() == true)

            }

        });


        chkSnapMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chkSnapMode.isChecked() == true) {

                    sp.edit().putBoolean("RotateSnapMode", true).commit();

                }
                else {

                    sp.edit().putBoolean("RotateSnapMode", false).commit();

                }//End if(chkSnapToCentre.isChecked() == true)

            }

        });


        btnCloseAndRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtEnterThreashold.setText(txtEnterThreashold.getText().toString().trim());

                if(txtEnterThreashold.getText().toString().equals("") == true) {

                    //Set Default Values
                    angleThreasholdVals[0] = 45;
                    angleThreasholdVals[1] = 0.5f;

                }

                String[] tempRotateThreashold = new String[2];

                Pattern pattern = Pattern.compile("\\d+\\s{0,},{1}\\s{0,}\\d+\\.{0,1}\\d{0,}", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(txtEnterThreashold.getText().toString());

                if(matcher.matches() == true) {

                    tempRotateThreashold = txtEnterThreashold.getText().toString().split("\\s{0,},{1,1}\\s{0,}", 2);

                    if(tempRotateThreashold.length == 2) {

                        angleThreasholdVals[0] = Float.parseFloat(tempRotateThreashold[0]);
                        angleThreasholdVals[1] = Float.parseFloat(tempRotateThreashold[1]);

                        sp.edit().putFloat("SnapToAngle", angleThreasholdVals[0]).commit();
                        sp.edit().putFloat("AngleSnapTolerance", angleThreasholdVals[1]).commit();

                    }
                    else {//Fault Tolerance

                        //Set Default Values
                        angleThreasholdVals[0] = 45;
                        angleThreasholdVals[1] = 0.5f;

                    }

                }
                else {//Fault Tolerance

                    //Set Default Values
                    angleThreasholdVals[0] = 45;
                    angleThreasholdVals[1] = 0.5f;

                }

                dialogClickListener.onClick(getDialog(), 0);

            }

        });

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState)

}