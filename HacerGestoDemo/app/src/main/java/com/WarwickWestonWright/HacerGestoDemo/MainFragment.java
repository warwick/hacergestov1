package com.WarwickWestonWright.HacerGestoDemo;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGesto.HGFlags;
import com.WarwickWestonWright.HacerGesto.HGFling;
import com.WarwickWestonWright.HacerGesto.HGMove;
import com.WarwickWestonWright.HacerGesto.HGResult;
import com.WarwickWestonWright.HacerGesto.HGRotate;
import com.WarwickWestonWright.HacerGesto.HGScale;
import com.WarwickWestonWright.HacerGesto.HacerGesto;

import java.io.IOException;
import java.io.InputStream;

import warwickwestonwright.hacergestodemo.R;

public class MainFragment extends Fragment implements DialogInterface.OnClickListener {

    private View rootView;
    private ImageView ImgMainFragmentTestTouch;
    private TextView LblMainFragmentGestureStatus;
    private AssetManager assetManager;
    private InputStream assetStream;
    private Bitmap bitmap;
    private Point point;
    private HacerGesto hacerGesto;
    private HGRotate hgRotate;
    private HGScale hgScale;
    private HGMove hgMove;
    private HGFling hgFling;
    private SharedPreferences sp;

    public MainFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.main_fragment, container, false);

        ImgMainFragmentTestTouch = (ImageView) rootView.findViewById(R.id.ImgMainFragmentTestTouch);
        LblMainFragmentGestureStatus = (TextView) rootView.findViewById(R.id.LblMainFragmentGestureStatus);

        assetManager = getActivity().getAssets();

        try {

            assetStream = assetManager.open("HacerGestoDeveloper.png");
            //assetStream = assetManager.open("HacerGestoDeveloper_right.png");
            /* If mutable bitmap is needed uncomment this section. Does not work on lower API
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inMutable = true;//Uncomment for mutable bitmap
            bitmap = BitmapFactory.decodeStream(assetStream, null, bitmapOptions);
            */
            bitmap = BitmapFactory.decodeStream(assetStream);
            bitmap = getProportionalBitmap(bitmap, point.x / 2, "X");

        }
        catch (IOException e) {

            e.printStackTrace();

        }

        ImgMainFragmentTestTouch.setImageBitmap(bitmap);

        //Top of Top Block.
        hacerGesto = new HacerGesto(ImgMainFragmentTestTouch, HGFlags.ROTATE | HGFlags.SCALE | HGFlags.MOVE | HGFlags.FLING) {
            @Override
            public void onDown(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onMotion(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onRelease(HGResult hgResult) {

                //These four if bocks are what you would use to capture values from each gesture and can be put in any override.
                if((hgResult.getWhatGesture() & HGFlags.ROTATE) == HGFlags.ROTATE) {//Constant Value 8

                    //This is the result of the Rotate Gesture.

                }

                if((hgResult.getWhatGesture() & HGFlags.SCALE) == HGFlags.SCALE) {//Constant Value 4

                    //This is the result of the Rotate Scale.

                }

                if((hgResult.getWhatGesture() & HGFlags.MOVE) == HGFlags.MOVE) {//Constant Value 1

                    //This is the result of the Rotate Move.

                }

                if((hgResult.getWhatGesture() & HGFlags.FLING) == HGFlags.FLING) {//Constant Value 2

                    //This is the result of the Rotate Fling.

                }//End if((hgResult.getWhatGesture() & HGFlags.ROTATE) == HGFlags.ROTATE)

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

        };

        ImgMainFragmentTestTouch.setOnTouchListener(hacerGesto);

        //expandPaddingToFitContainer is a convenience method that expands the padding to the edge of the it's parent view. Setting to false will set the padding to zero.
        //If you wish to use irregular padding in the image view do NOT call this method.
        hacerGesto.expandPaddingToFitContainer(true);
        hacerGesto.disableGesture(true, HGFlags.ROTATE | HGFlags.SCALE | HGFlags.MOVE | HGFlags.FLING);

        //Restore flages for all gestures.
        onClick(null, 3);
        onClick(null, 2);
        onClick(null, 1);
        onClick(null, 0);
        //Bottom of Top Block.

        //These four blocks of commented out code below are for using each gesture individually outside the library.
        //when using more than one outside the library you will need to refer to the extensibility notes to understand how they work together. (https://bitbucket.org/warwick/hacergestov1)
        //Also when using these classes outside the package you will need to include the dependency class HGResult.
        //If you uncomment any one or more of the blocks below you will need to comment out the block above labelled 'Top Block.'
        //Please read the comments on how to use the fling gestures properly by reading the 'triggerFling method usage notes:'
        //They are located in the comments within the commented out fling gesture below.
        //The behavioural methods for each gesture are described in the comments for each gesture section below.
        //For your convenience, all of the behavioural methods may be called through the HacerGesto object


        /*
        hgMove = new HGMove(ImgMainFragmentTestTouch) {
            @Override
            public void onDown(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onMotion(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onRelease(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

        };

        ImgMainFragmentTestTouch.setOnTouchListener(hgMove);

        //expandPaddingToFitContainer is a convenience method that expands the padding to the edge of the it's parent view. Setting to false will set the padding to zero.
        //If you wish to use irregular padding in the image view do NOT call this method.
        hgMove.expandPaddingToFitContainer(true);
        //setDisableGesture method dynamically disable the gesture when set to true and re enables it when set to false
        hgMove.setDisableGesture(false);
        //setCumaulativeMove when true image move relative to touch, false causes the move to start from where the finger goes down.
        hgMove.setCumaulativeMove(true);
        //manualMove method simply moves to an XY coordinate very handy to use in the gesture overrides combined with hgResult.getXPos/getYPos and setDisableGesture methods.
        //hgMove.manualMove(270, 493);

        //Top of move snap block replace values with your XY snap positions
        //setMoveSnap snaps to XY positions from the first parameter and the second parameter contain the snapping proximity tolerance.
        //float[] myTolerances =  {50, 100, 150, 200};
        //Point[] myPositions = new Point[4];
        //myPositions[0] = new Point(-270, -493);
        //myPositions[1] = new Point(270, -493);
        //myPositions[2] = new Point(270, 493);
        //myPositions[3] = new Point(-270, 493);
        //hgMove.setMoveSnap(myPositions, myTolerances);
        //Bottom of move snap block replace values with your XY snap positions
        */


        /* Flinging
        hgFling = new HGFling(ImgMainFragmentTestTouch) {
            @Override
            public void onDown(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onMotion(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onRelease(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));
                int paddingLeft = ImgMainFragmentTestTouch.getPaddingLeft();
                int paddingTop = ImgMainFragmentTestTouch.getPaddingTop();
                //Object usage demonstration: uncomment any ONE of the lines below to demonstrate how you can override the default behaviour of this fling gesture.
                //hgFling.triggerFling((int) hgResult.getTwoFingerDistance(0, 0, paddingLeft, paddingTop), -(paddingLeft), -(paddingTop));//Uncomment this line to fling to UPPER LEFT corner
                //hgFling.triggerFling((int) hgResult.getTwoFingerDistance(0, 0, paddingLeft, paddingTop), (paddingLeft), -(paddingTop));//Uncomment this line to fling to UPPER RIGHT corner
                //hgFling.triggerFling((int) hgResult.getTwoFingerDistance(0, 0, paddingLeft, paddingTop), -(paddingLeft), (paddingTop));//Uncomment this line to fling to LOWER LEFT corner
                //hgFling.triggerFling((int) hgResult.getTwoFingerDistance(0, 0, paddingLeft, paddingTop), (paddingLeft), (paddingTop));//Uncomment this line to fling to LOWER RIGHT corner

                //'triggerFling method usage notes:'
                //When the first parameter is set to zero within this override the image will be flung to an arbitrary XY position dictated by the 2nd and 3rd parameter respectively
                //When the first parameter is non zero the gesture will fling the distance in pixels in the proportional direction set by the 2nd and 3rd parameter.
                //To enhance this usage a function (getTwoFingerDistance) will calculate the diagonal distance between any top points.
                //To view a demonstration of this just uncomment ONE of the four lines of code above. It will fling the image to one of the corners; assuming the image has centred padding.
                //This method only works when called from this override and is unsafe to call anywhere else.

            }

        };

        ImgMainFragmentTestTouch.setOnTouchListener(hgFling);

        //expandPaddingToFitContainer is a convenience method that expands the padding to the edge of the it's parent view. Setting to false will set the padding to zero.
        //If you wish to use irregular padding in the image view do NOT call this method.
        hgFling.expandPaddingToFitContainer(true);
        //setDisableGesture method dynamically disable the gesture when set to true and re enables it when set to false
        //hgFling.setDisableGesture(true);
        //setFlingDistance causes the gesture to fling the distance in pixels set by the parameter. this does not have to be called from any of the overrides.
        //hgFling.setFlingDistance(350);
        //setupFlingValues sets the fling threshold values. 1st parameter sets fling distance, the 2nd is the fling time and the 3rd is the speed of the animation.
        //if the gesture moves a distance more than the distance in pixels in the time set in milliseconds in the 2nd parameter and the gesture released then the gesture will be triggered
        hgFling.setupFlingValues(200, 300, 500);

        //Top of Block usage of triggerFling outside the overrides.
        //int paddingLeft = ImgMainFragmentTestTouch.getPaddingLeft();
        //int paddingTop = ImgMainFragmentTestTouch.getPaddingTop();
        //HGResult hgResult = new HGResult();
        //hgFling.triggerFling((int) hgResult.getTwoFingerDistance(0, 0, paddingLeft, paddingTop), -270, -493);//You need not use trigger fling in the overrides.
        //Bottom of Block usage of triggerFling outside the overrides.

        //setFlingBounceBack causes the image to bounce back to it's original position after the gesture is complete. This method was included for test purposes.
        hgFling.setFlingBounceBack(true);
        */


        /*
        HGScale hgScale = new HGScale(ImgMainFragmentTestTouch) {
            @Override
            public void onDown(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onMotion(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onRelease(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

        };

        ImgMainFragmentTestTouch.setOnTouchListener(hgScale);

        //expandPaddingToFitContainer is a convenience method that expands the padding to the edge of the it's parent view. Setting to false will set the padding to zero.
        //If you wish to use irregular padding in the image view do NOT call this method.
        hgScale.expandPaddingToFitContainer(true);
        //setDisableGesture method dynamically disable the gesture when set to true and re enables it when set to false
        hgScale.setDisableGesture(false);
        //setSnapToCenter is used with the move gesture. When true image will snap to center when scaling. When false scaling will happen relative to it's current position.
        hgScale.setSnapToCenter(false);
        //manualScale method is a simple convenience method that manually scales the image.
        //hgScale.manualScale(2, 2);
        //setMinMaxSnap method causes snapping behaviour. 1st parameter is minimum scale size, 2nd is maximum and the 3rd is the tolerance. When last parameter is 0 no snapping occurs but
        //minimum and maximum sizes are enforced. When first two parameters are zero it simply snaps ever amount of pixels from third parameter. To disable snapping set all parameters to zero.
        //hgScale.setMinMaxSnap(350, 1920, 100);
        //getProportionalBitmap is a convenience method than returns a proportionally scaled bitmap. 1st parameter is the bitmap, 2nd is the new width or height the 3rd can be X or Y which tells the method
        //if the 2nd parameter is intended as a new width or a new height.
        //hgScale.getProportionalBitmap(((BitmapDrawable)ImgMainFragmentTestTouch.getDrawable()).getBitmap(), 200, "x");
        */


        /*
        hgRotate = new HGRotate(ImgMainFragmentTestTouch) {
            @Override
            public void onDown(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onMotion(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

            @Override
            public void onRelease(HGResult hgResult) {

                LblMainFragmentGestureStatus.setText("X: " + Float.toString(hgResult.getXPos()) + "\nY: " + Float.toString(hgResult.getYPos()) + "\nScale: " + Float.toString(hgResult.getScaleX()) + "\nAngle: " + Float.toString(hgResult.getAngle()));

            }

        };

        ImgMainFragmentTestTouch.setOnTouchListener(hgRotate);

        //expandPaddingToFitContainer is a convenience method that expands the padding to the edge of the it's parent view. Setting to false will set the padding to zero.
        //If you wish to use irregular padding in the image view do NOT call this method.
        hgRotate.expandPaddingToFitContainer(true);
        //setDisableGesture method dynamically disable the gesture when set to true and re enables it when set to false
        hgRotate.setDisableGesture(false);
        //setSnapToCenter is used with the move gesture. When true image will snap to center when rotating. When false scaling will happen relative to it's current position.
        hgRotate.setSnapToCenter(false);
        //manualRotate is a simple method to rotate. 1st parameter is the angle and the 2nd is a Point object that dictates the rotation centre point.
        //hgRotate.manualRotate(90, new Point(ImgMainFragmentTestTouch.getMeasuredWidth() / 2, ImgMainFragmentTestTouch.getMeasuredHeight() / 2));
        //setAngleSnap causes the rotation to snap by degrees from 1st parameter. The 2nd parameter is a fraction of the 1st parameter. The example below will snap 45 degrees
        //for ever 22.5 degrees you rotate. To disable this gesture call the setAngleSnap and set the 1st parameter to zero.
        //hgRotate.setAngleSnap(45, 0.5f);
        //getAngleFromPoint is a convenience method than returns the angle between any two points and is used internally by the object.
        //hgRotate.getAngleFromPoint(new Point(0, 0), new Point(10, 10));
        //When setDialMode is set to true the rotate gesture is tuned into a single finger gesture. When false it returns to a two finger gesture.
        hgRotate.setDialMode(true);
        //Note: If you use this gesture with angle setAngleSnap set to true after first using it when set to false the HGResult will reflect the amount of degrees rotated without
        //adding the last angle returned by the gesture when the setAngleSnap was set to false. This is by design.
        */

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


    //Set XorY to "X" to adjust height or "Y" to adjust width. A public copy of this function is in the HGScale class as a convenience method.
    private Bitmap getProportionalBitmap(Bitmap bitmap, int newDimensionXorY, String XorY) {

        if(bitmap == null) {

            return null;

        }

        float xyRatio = 0;
        int newWidth = 0;
        int newHeight = 0;

        if(XorY.toLowerCase().equals("x")) {

            xyRatio = (float) newDimensionXorY / bitmap.getWidth();
            newHeight = (int) (bitmap.getHeight() * xyRatio);
            bitmap = Bitmap.createScaledBitmap(bitmap, newDimensionXorY, newHeight, true);

        }
        else if(XorY.toLowerCase().equals("y")) {

            xyRatio = (float) newDimensionXorY / bitmap.getHeight();
            newWidth = (int) (bitmap.getWidth() * xyRatio);
            bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newDimensionXorY, true);

        }

        return bitmap;

    }//End private Bitmap getProportionalBitmap(Bitmap bitmap, int newDimensionXorY, String XorY)


    @Override
    public void onClick(DialogInterface dialog, int which) {

        if(which == 0) {//Rotate

            if(sp.getBoolean("RotateEnabled", false) == true) {

                hacerGesto.disableGesture(false, HGFlags.ROTATE);

            }
            else {

                hacerGesto.disableGesture(true, HGFlags.ROTATE);

            }//End if(sp.getBoolean("RotateEnabled", false) == true)

            if(sp.getBoolean("RotateSnapToCentre", false) == true) {

                hacerGesto.setSnapToCenter(true, HGFlags.ROTATE);

            }
            else {

                hacerGesto.setSnapToCenter(false, HGFlags.ROTATE);

            }//End if(sp.getBoolean("RotateSnapToCentre", false) == true)

            if(sp.getBoolean("RotateDialMode", false) == true) {

                hacerGesto.setDialMode(true);

            }
            else {

                hacerGesto.setDialMode(false);

            }//End if(sp.getBoolean("RotateDialMode", false) == true)

            if(sp.getBoolean("RotateSnapMode", false) == true) {

                hacerGesto.setAngleSnap(sp.getFloat("SnapToAngle", 0), sp.getFloat("AngleSnapTolerance", 0));

            }
            else {

                hacerGesto.setAngleSnap(0, 0);

            }//End if(sp.getBoolean("RotateSnapMode", false) == true)

        }
        else if(which == 1) {//Scale

            if(sp.getBoolean("ScaleEnabled", false) == true) {

                hacerGesto.disableGesture(false, HGFlags.SCALE);

            }
            else {

                hacerGesto.disableGesture(true, HGFlags.SCALE);

            }//End if(sp.getBoolean("ScaleEnabled", false) == true)

            if(sp.getBoolean("ScaleSnapToCentre", false) == true) {

                hacerGesto.setSnapToCenter(true, HGFlags.SCALE);

            }
            else {

                hacerGesto.setSnapToCenter(false, HGFlags.SCALE);

            }//End if(sp.getBoolean("ScaleSnapToCentre", false) == true)

            if(sp.getBoolean("ScaleSnappingEnabled", false) == true) {

                if(sp.getFloat("MinimumSnap", 0) == 0  && sp.getFloat("MaximumSnap", 0) == 0 && sp.getFloat("ScaleThreashold", 0) == 0) {

                    hacerGesto.setMinMaxSnap(sp.getFloat("MinimumSnap", 0), sp.getFloat("MaximumSnap", 0), sp.getFloat("ScaleThreashold", 0));

                }
                else if(sp.getFloat("MinimumSnap", 0) != 0  && sp.getFloat("MaximumSnap", 0) != 0 && sp.getFloat("ScaleThreashold", 0) == 0) {

                    hacerGesto.setMinMaxSnap(sp.getFloat("MinimumSnap", 0), sp.getFloat("MaximumSnap", 0), sp.getFloat("ScaleThreashold", 0));

                }
                else if(sp.getFloat("MinimumSnap", 0) == 0  && sp.getFloat("MaximumSnap", 0) == 0 && sp.getFloat("ScaleThreashold", 0) != 0) {

                    hacerGesto.setMinMaxSnap(sp.getFloat("MinimumSnap", 0), sp.getFloat("MaximumSnap", 0), sp.getFloat("ScaleThreashold", 0));

                }
                else if(sp.getFloat("MinimumSnap", 0) != 0  && sp.getFloat("MaximumSnap", 0) != 0 && sp.getFloat("ScaleThreashold", 0) != 0) {

                    hacerGesto.setMinMaxSnap(sp.getFloat("MinimumSnap", 0), sp.getFloat("MaximumSnap", 0), sp.getFloat("ScaleThreashold", 0));

                }
                else {

                    hacerGesto.setMinMaxSnap(0, 0, 0);

                }//End if(sp.getFloat("MinimumSnap", 0) == 0  && sp.getFloat("MaximumSnap", 0) == 0 && sp.getFloat("ScaleThreashold", 0) == 0)

            }
            else {

                hacerGesto.setMinMaxSnap(0, 0, 0);

            }//End if(sp.getBoolean("ScaleSnappingEnabled", false) == true)

        }
        else if(which == 2) {//Move

            if(sp.getBoolean("MoveEnabled", false) == true) {

                hacerGesto.disableGesture(false, HGFlags.MOVE);

            }
            else {

                hacerGesto.disableGesture(true, HGFlags.MOVE);

            }//End if(sp.getBoolean("MoveEnabled", false) == true)

            if(sp.getBoolean("CumulativeMove", false) == true) {

                hacerGesto.setCumaulativeMove(true);

            }
            else {

                hacerGesto.setCumaulativeMove(false);

            }//End if(sp.getBoolean("CumulativeMove", false) == true)

            if(sp.getBoolean("MoveSnapping", false) == true) {

                Point[] moveSnapPosition = new Point[1];
                moveSnapPosition[0] = new Point((int) sp.getFloat("MoveSnappingPositionX", 0), (int) sp.getFloat("MoveSnappingPositionY", 0));
                float[] moveSnapTolerance = new float[1];
                moveSnapTolerance[0] = sp.getFloat("MoveSnappingTolerance", 0);
                hacerGesto.setMoveSnap(moveSnapPosition, moveSnapTolerance);
                LblMainFragmentGestureStatus.setText("Your screen dimensions are:\nWidth: " +
                        Integer.toString(getActivity().getResources().getDisplayMetrics().widthPixels) +
                        "\nHeight: " + Integer.toString(getActivity().getResources().getDisplayMetrics().heightPixels));

            }
            else {

                hacerGesto.setMoveSnap(null, null);

            }//End if(sp.getBoolean("MoveSnapping", false) == true)

        }
        else if(which == 3) {//Fling

            if(sp.getBoolean("FlingEnabled", false) == true) {

                hacerGesto.disableGesture(false, HGFlags.FLING);

            }
            else {

                hacerGesto.disableGesture(true, HGFlags.FLING);

            }//End if(sp.getBoolean("FlingEnabled", false) == true)

            if(sp.getBoolean("BounceBack", false) == true) {

                hacerGesto.setFlingBounceBack(true);

            }
            else {

                hacerGesto.setFlingBounceBack(false);

            }//End if(sp.getBoolean("BounceBack", false) == true)

            if(sp.getInt("FlingDistance", 0) == 0) {

                hacerGesto.setFlingDistance(0);

            }
            else {

                hacerGesto.setFlingDistance(sp.getInt("FlingDistance", 0));

            }//End if(sp.getInt("FlingDistance", 0) == 0)

            if(sp.getInt("FlingDistanceThreashold", 0) != 0 && sp.getInt("FlingTimeThreashold", 0) != 0 && sp.getInt("FlingAnimationTime", 0) != 0) {

                hacerGesto.setupFlingValues(sp.getInt("FlingDistanceThreashold", 0), sp.getInt("FlingTimeThreashold", 0), sp.getInt("FlingAnimationTime", 0));

            }
            else {

                hacerGesto.setupFlingValues(0, 0, 0);

            }//End if(sp.getInt("FlingDistanceThreashold", 0) != 0 && sp.getInt("FlingTimeThreashold", 0) != 0 && sp.getInt("FlingAnimationTime", 0) != 0)

        }//End if(which == 0)

        if(dialog != null) {

            dialog.dismiss();

        }

    }//End public void onClick(DialogInterface dialog, int which)

}