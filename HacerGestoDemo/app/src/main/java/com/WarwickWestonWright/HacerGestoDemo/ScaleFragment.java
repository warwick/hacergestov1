package com.WarwickWestonWright.HacerGestoDemo;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import warwickwestonwright.hacergestodemo.R;

public class ScaleFragment extends DialogFragment {

    private DialogInterface.OnClickListener dialogClickListener;

    private SharedPreferences sp;

    //Declare Widgets
    private Button btnCloseAndScale;
    private CheckBox chkEnable;
    private CheckBox chkSnapToCentre;
    private CheckBox chkScaleSnapping;
    private EditText txtEnterSnappingThreashold;
    private View rootView;

    //private float[] angleThreasholdVals;
    private float[] scaleThreasholdVals;

    public ScaleFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scaleThreasholdVals = new float[3];

        try {

            dialogClickListener = (DialogInterface.OnClickListener) getTargetFragment();

        }
        catch (ClassCastException e) {

            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");

        }

    }//End public void onCreate(Bundle savedInstanceState)


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.scale_fragment, container, false);

        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //Instantiate Widgets
        btnCloseAndScale = (Button) rootView.findViewById(R.id.btnCloseAndScale);
        chkEnable = (CheckBox) rootView.findViewById(R.id.chkEnable);
        chkSnapToCentre = (CheckBox) rootView.findViewById(R.id.chkSnapToCentre);
        chkScaleSnapping = (CheckBox) rootView.findViewById(R.id.chkScaleSnapping);
        chkEnable.setChecked(sp.getBoolean("ScaleEnabled", false));
        chkSnapToCentre.setChecked(sp.getBoolean("ScaleSnapToCentre", false));
        chkScaleSnapping.setChecked(sp.getBoolean("ScaleSnappingEnabled", false));
        txtEnterSnappingThreashold = (EditText) rootView.findViewById(R.id.txtEnterSnappingThreashold);

        String tempThreashold = Integer.toString(((int) sp.getFloat("MinimumSnap", 0))) + ", " +
                Integer.toString(((int) sp.getFloat("MaximumSnap", 0))) + ", " +
                Integer.toString(((int) sp.getFloat("ScaleThreashold", 0)));

        txtEnterSnappingThreashold.setText(tempThreashold);

        chkEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chkEnable.isChecked() == true) {

                    sp.edit().putBoolean("ScaleEnabled", true).commit();

                } else {

                    sp.edit().putBoolean("ScaleEnabled", false).commit();

                }//End if(chkEnable.isChecked() == true)

            }

        });


        chkSnapToCentre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chkSnapToCentre.isChecked() == true) {

                    sp.edit().putBoolean("ScaleSnapToCentre", true).commit();

                }
                else {

                    sp.edit().putBoolean("ScaleSnapToCentre", false).commit();

                }//End if(chkSnapToCentre.isChecked() == true)

            }

        });


        chkScaleSnapping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chkScaleSnapping.isChecked() == true) {

                    sp.edit().putBoolean("ScaleSnappingEnabled", true).commit();

                }
                else {

                    sp.edit().putBoolean("ScaleSnappingEnabled", false).commit();

                }//End if(chkSnapToCentre.isChecked() == true)

            }

        });


        btnCloseAndScale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtEnterSnappingThreashold.setText(txtEnterSnappingThreashold.getText().toString().trim());

                String[] tempScaleThreashold = new String[3];

                Pattern pattern = Pattern.compile("\\d+\\s{0,},{1,1}\\s{0,}\\d+\\s{0,},{1,1}\\s{0,}\\d+", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(txtEnterSnappingThreashold.getText().toString());

                if(matcher.matches() == true) {

                    tempScaleThreashold = txtEnterSnappingThreashold.getText().toString().split("\\s{0,},{1,1}\\s{0,}", 3);

                    if(tempScaleThreashold.length == 3) {

                        scaleThreasholdVals[0] = Float.parseFloat(tempScaleThreashold[0]);
                        scaleThreasholdVals[1] = Float.parseFloat(tempScaleThreashold[1]);
                        scaleThreasholdVals[2] = Float.parseFloat(tempScaleThreashold[2]);

                        sp.edit().putFloat("MinimumSnap", scaleThreasholdVals[0]).commit();
                        sp.edit().putFloat("MaximumSnap", scaleThreasholdVals[1]).commit();
                        sp.edit().putFloat("ScaleThreashold", scaleThreasholdVals[2]).commit();

                    }
                    else {//Fault Tolerance

                        //Set Default Values
                        scaleThreasholdVals[0] = 350;
                        scaleThreasholdVals[1] = 1080;
                        scaleThreasholdVals[2] = 100;

                    }//End if(tempFlingThreashold.length == 3)

                }
                else {//Fault Tolerance

                    //Set Default Values
                    scaleThreasholdVals[0] = 350;
                    scaleThreasholdVals[1] = 1080;
                    scaleThreasholdVals[2] = 100;

                }//End if(matcher.matches() == true)

                dialogClickListener.onClick(getDialog(), 1);

            }

        });

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState)

}