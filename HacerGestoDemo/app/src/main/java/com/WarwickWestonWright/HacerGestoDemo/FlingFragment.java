package com.WarwickWestonWright.HacerGestoDemo;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import warwickwestonwright.hacergestodemo.R;

public class FlingFragment extends DialogFragment {

    private DialogInterface.OnClickListener dialogClickListener;

    private SharedPreferences sp;
    private Button btnCloseAndFling;
    private CheckBox chkEnable;
    private CheckBox chkBounceBack;
    private EditText txtFlingDistance;
    private EditText txtEnterThreashold;
    private int flingDistance = 0;
    private int[] flingThreasholdVals;
    private View rootView;

    public FlingFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        flingThreasholdVals = new int[3];

        try {

            dialogClickListener = (DialogInterface.OnClickListener) getTargetFragment();

        }
        catch (ClassCastException e) {

            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");

        }

    }//End public void onCreate(Bundle savedInstanceState)


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fling_fragment, container, false);

        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //Instantiate Widgets
        btnCloseAndFling = (Button) rootView.findViewById(R.id.btnCloseAndFling);
        chkEnable = (CheckBox) rootView.findViewById(R.id.chkEnable);
        chkBounceBack = (CheckBox) rootView.findViewById(R.id.chkBounceBack);
        txtFlingDistance = (EditText) rootView.findViewById(R.id.txtFlingDistance);
        txtEnterThreashold = (EditText) rootView.findViewById(R.id.txtEnterThreashold);;

        if(sp.getInt("FlingDistanceThreashold", 0) != 0 && sp.getInt("FlingTimeThreashold", 0) != 0 && sp.getInt("FlingAnimationTime", 0) != 0) {

            txtEnterThreashold.setText(Integer.toString(sp.getInt("FlingDistanceThreashold", 0)) + ", " + Integer.toString(sp.getInt("FlingTimeThreashold", 0)) + ", " + Integer.toString(sp.getInt("FlingAnimationTime", 0)));

        }
        else {

            txtEnterThreashold.setText("250, 280, 500");

        }//End if(sp.getInt("FlingDistanceThreashold", 0) != 0 && sp.getInt("FlingTimeThreashold", 0) != 0 && sp.getInt("FlingAnimationTime", 0) != 0)

        txtFlingDistance.setText(Integer.toString(sp.getInt("FlingDistance", 0)));

        chkEnable.setChecked(sp.getBoolean("FlingEnabled", false));
        chkBounceBack.setChecked(sp.getBoolean("BounceBack", false));

        chkEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chkEnable.isChecked() == true) {

                    sp.edit().putBoolean("FlingEnabled", true).commit();

                } else {

                    sp.edit().putBoolean("FlingEnabled", false).commit();

                }//End if(chkEnable.isChecked() == true)

            }

        });


        chkBounceBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chkBounceBack.isChecked() == true) {

                    sp.edit().putBoolean("BounceBack", true).commit();

                }
                else {

                    sp.edit().putBoolean("BounceBack", false).commit();

                }//End if(chkEnable.isChecked() == true)

            }

        });


        btnCloseAndFling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtFlingDistance.setText(txtFlingDistance.getText().toString().trim());
                txtEnterThreashold.setText(txtEnterThreashold.getText().toString().trim());

                if(txtFlingDistance.getText().toString().trim().equals("") == true) {

                    txtFlingDistance.setText("0");
                    sp.edit().putInt("FlingDistance", 0).commit();

                }
                else {

                    sp.edit().putInt("FlingDistance", Integer.parseInt(txtFlingDistance.getText().toString())).commit();

                }

                flingDistance = Integer.parseInt(txtFlingDistance.getText().toString());

                String[] tempFlingThreashold = new String[3];

                Pattern pattern = Pattern.compile("\\d+\\s{0,},{1,1}\\s{0,}\\d+\\s{0,},{1,1}\\s{0,}\\d+", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(txtEnterThreashold.getText().toString());

                if(matcher.matches() == true) {

                    tempFlingThreashold = txtEnterThreashold.getText().toString().split("\\s{0,},{1,1}\\s{0,}", 3);

                    if(tempFlingThreashold.length == 3) {

                        flingThreasholdVals[0] = Integer.parseInt(tempFlingThreashold[0]);
                        flingThreasholdVals[1] = Integer.parseInt(tempFlingThreashold[1]);
                        flingThreasholdVals[2] = Integer.parseInt(tempFlingThreashold[2]);

                        sp.edit().putInt("FlingDistanceThreashold", flingThreasholdVals[0]).commit();
                        sp.edit().putInt("FlingTimeThreashold", flingThreasholdVals[1]).commit();
                        sp.edit().putInt("FlingAnimationTime", flingThreasholdVals[2]).commit();

                    }
                    else {//Fault Tolerance

                        //Set Default Values
                        flingThreasholdVals[0] = 250;
                        flingThreasholdVals[1] = 280;
                        flingThreasholdVals[2] = 500;

                    }//End if(tempFlingThreashold.length == 3)

                }
                else {//Fault Tolerance

                    //Set Default Values
                    flingThreasholdVals[0] = 250;
                    flingThreasholdVals[1] = 280;
                    flingThreasholdVals[2] = 500;

                }//End if(matcher.matches() == true)

                dialogClickListener.onClick(getDialog(), 3);

            }

        });

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState)

}