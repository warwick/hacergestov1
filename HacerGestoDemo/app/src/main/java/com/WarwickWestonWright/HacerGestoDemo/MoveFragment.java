package com.WarwickWestonWright.HacerGestoDemo;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import warwickwestonwright.hacergestodemo.R;

public class MoveFragment extends DialogFragment {

    private DialogInterface.OnClickListener dialogClickListener;

    private SharedPreferences sp;
    private Button btnCloseAndMove;
    private CheckBox chkEnable;
    private CheckBox chkCumulativeMove;
    private CheckBox chkSnapping;
    private EditText txtMoveSnappingValues;
    private float[] moveSnappingVals;
    private View rootView;

    public MoveFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        moveSnappingVals = new float[3];

        try {

            dialogClickListener = (DialogInterface.OnClickListener) getTargetFragment();

        }
        catch (ClassCastException e) {

            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");

        }

    }//End public void onCreate(Bundle savedInstanceState)


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.move_fragment, container, false);

        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

        btnCloseAndMove = (Button) rootView.findViewById(R.id.btnCloseAndMove);
        chkEnable = (CheckBox) rootView.findViewById(R.id.chkEnable);
        chkCumulativeMove = (CheckBox) rootView.findViewById(R.id.chkCumulativeMove);
        chkSnapping = (CheckBox) rootView.findViewById(R.id.chkSnapping);
        txtMoveSnappingValues = (EditText) rootView.findViewById(R.id.txtMoveSnappingValues);

        String tempSnappingValues = Integer.toString(((int) sp.getFloat("MoveSnappingPositionX", 0))) + ", " +
                Integer.toString(((int) sp.getFloat("MoveSnappingPositionY", 0))) + ", " +
                Integer.toString(((int) sp.getFloat("MoveSnappingTolerance", 0)));

        txtMoveSnappingValues.setText(tempSnappingValues);

        chkEnable.setChecked(sp.getBoolean("MoveEnabled", false));
        chkCumulativeMove.setChecked(sp.getBoolean("CumulativeMove", false));
        chkSnapping.setChecked(sp.getBoolean("MoveSnapping", false));

        chkEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chkEnable.isChecked() == true) {

                    sp.edit().putBoolean("MoveEnabled", true).commit();

                }
                else {

                    sp.edit().putBoolean("MoveEnabled", false).commit();

                }//End if(chkEnable.isChecked() == true)

            }

        });


        chkCumulativeMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chkCumulativeMove.isChecked() == true) {

                    sp.edit().putBoolean("CumulativeMove", true).commit();

                }
                else {

                    sp.edit().putBoolean("CumulativeMove", false).commit();

                }//End if(chkSnapToCentre.isChecked() == true)

            }

        });


        chkSnapping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chkSnapping.isChecked() == true) {

                    sp.edit().putBoolean("MoveSnapping", true).commit();

                }
                else {

                    sp.edit().putBoolean("MoveSnapping", false).commit();

                }//End if(chkSnapToCentre.isChecked() == true)

            }

        });


        btnCloseAndMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtMoveSnappingValues.setText(txtMoveSnappingValues.getText().toString().trim());

                if(txtMoveSnappingValues.equals("") == true) {

                    txtMoveSnappingValues.setText("0, 0, 0");

                }

                String[] tempSnappingText = new String[3];

                Pattern pattern = Pattern.compile("-?\\d+\\s{0,},{1,1}\\s{0,}-?\\d+\\s{0,},{1,1}\\s{0,}\\d+", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(txtMoveSnappingValues.getText().toString());

                if(matcher.matches() == true) {

                    tempSnappingText = txtMoveSnappingValues.getText().toString().split("\\s{0,},{1,1}\\s{0,}", 3);

                    if(tempSnappingText.length == 3) {

                        moveSnappingVals[0] = Float.parseFloat(tempSnappingText[0]);
                        moveSnappingVals[1] = Float.parseFloat(tempSnappingText[1]);
                        moveSnappingVals[2] = Float.parseFloat(tempSnappingText[2]);

                        sp.edit().putFloat("MoveSnappingPositionX", moveSnappingVals[0]).commit();
                        sp.edit().putFloat("MoveSnappingPositionY", moveSnappingVals[1]).commit();
                        sp.edit().putFloat("MoveSnappingTolerance", moveSnappingVals[2]).commit();

                    }
                    else {//Fault Tolerance

                        //Set Default Values
                        moveSnappingVals[0] = 0;
                        moveSnappingVals[1] = 0;
                        moveSnappingVals[2] = 100;

                    }//End if(tempFlingThreashold.length == 3)

                }
                else {//Fault Tolerance

                    //Set Default Values
                    moveSnappingVals[0] = 0;
                    moveSnappingVals[1] = 0;
                    moveSnappingVals[2] = 100;

                }//End if(matcher.matches() == true)

                dialogClickListener.onClick(getDialog(), 2);

            }

        });

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState)

}