package com.WarwickWestonWright.HacerGesto;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

public class HacerGestoDown {

    private Rect rect = new Rect();
    private static HGResult hgResult;
    private int hgFlag;

    public HacerGestoDown(View view, int hgFlag) {

        this.hgFlag = hgFlag;

    }//End public HacerGestoDown(View view, int hgFlag)

    private IHacerGestoDown iHacerGestoDown;

    public interface IHacerGestoDown {

        public void onDown(HGResult hgResult);

    }


    public HGResult getHgResult(MotionEvent event, int hgFlag) {

        return hgResult;

    }

}