/*
Notes about this class: The encapsulation between this class and the other gesture classes is absolute with the exception of the setMatrixVals(float[] matrixVals) method.
When extending this class please refer the the *Extensibility notes:* in the overview section of the repo at: https://bitbucket.org/warwick/hacergestov1
There is a section in this class with a comment block '//Top of block to call extendible behaviour methods that may be added to this class'
It is in this section that I (the developer) recommend extensible methods should be called. There is a function here 'getSnapMotion(event)'. Examining how this function
is called will give you a clear idea of how this class was designed to be extended.
For the sake of keeping the code clean, methods should be added at the block starting with the comment '//Top of Block behavioural methods'
*/
package com.WarwickWestonWright.HacerGesto;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public abstract class HGScale implements View.OnTouchListener, HacerGestoDown.IHacerGestoDown, HacerGestoMove.IHacerGestoMove, HacerGestoUp.IHacerGestoUp {

    private static boolean disableGestureInternally = false;
    private static boolean disableGestureDynamically = false;
    private static boolean suppressInvalidaton = false;
    private static float minimumSnap = 0;
    private static float maximumSnap = 0;
    private static float snapTolerance = 0;
    private static float sizeBeforeSnapActionX = 0;
    private static float sizeBeforeSnapActionY = 0;
    private static boolean minMaxSnapIsBeingUsed = false;
    private static boolean overOrUnderScaled = false;
    private static boolean hasbeenManuallyScaledOnDown = false;

    private ImageView imageView = null;
    private HGResult hgResult = null;
    private Matrix matrix = null;
    private static float[] matrixVals = null;
    private static int measureSpec = 0;

    private static boolean snapToCenter = false;
    private static boolean paddingIsExpanded = false;
    private static boolean scaleunderZeroX = false;
    private static boolean scaleunderZeroY = false;

    private static float moveTransX = 0;
    private static float moveTransY = 0;

    private static int firstPointerId = 0;
    private static int secondPointerId = 0;
    private static int firstPointerIdx = 0;
    private static int secondPointerIdx = 0;

    private static int origonalWidth = 0;
    private static int origonalHeight = 0;
    private static int paddingL = 0;
    private static int paddingT = 0;
    private static int paddingR = 0;
    private static int paddingB = 0;

    private static float firstTouchX = 0;
    private static float firstTouchY = 0;
    private static float secondTouchX = 0;
    private static float secondTouchY = 0;
    private static float pinchStartDistance = 0;
    private static float pinchCurrentDistance = 0;

    private HacerGestoDown.IHacerGestoDown iHacerGestoDown = null;
    private HacerGestoMove.IHacerGestoMove iHacerGestoMove = null;
    private HacerGestoUp.IHacerGestoUp iHacerGestoUp = null;

    private static HacerGestoDown hacerGestoDown = null;
    private static HacerGestoMove hacerGestoMove = null;
    private static HacerGestoUp hacerGestoUp = null;

    public HGScale(ImageView imageView) {

        disableGestureInternally = false;
        disableGestureDynamically = false;
        suppressInvalidaton = false;
        minimumSnap = 0;
        maximumSnap = 0;
        snapTolerance = 0;
        sizeBeforeSnapActionX = 0;
        sizeBeforeSnapActionY = 0;
        minMaxSnapIsBeingUsed = false;
        overOrUnderScaled = false;
        hasbeenManuallyScaledOnDown = false;

        paddingIsExpanded = false;

        snapToCenter = false;
        scaleunderZeroX = false;
        scaleunderZeroY = false;

        moveTransX = 0;
        moveTransY = 0;

        matrixVals = new float[9];
        origonalWidth = 0;
        origonalHeight = 0;
        paddingL = imageView.getPaddingLeft();
        paddingT = imageView.getPaddingTop();
        paddingR = imageView.getPaddingRight();
        paddingB = imageView.getPaddingBottom();

        firstTouchX = 0;
        firstTouchY = 0;
        secondTouchX = 0;
        secondTouchY = 0;
        pinchStartDistance = 0;
        pinchCurrentDistance = 0;

        firstPointerId = 0;
        secondPointerId = 0;
        firstPointerIdx = 0;
        secondPointerIdx = 0;

        measureSpec = View.MeasureSpec.getSize(View.MeasureSpec.UNSPECIFIED);
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        imageView.measure(measureSpec, measureSpec);

        iHacerGestoDown = this;
        iHacerGestoMove = this;
        iHacerGestoUp = this;

        hacerGestoDown = new HacerGestoDown(imageView, HGFlags.MOVE) {};
        hacerGestoMove = new HacerGestoMove(imageView, HGFlags.MOVE) {};
        hacerGestoUp = new HacerGestoUp(imageView, HGFlags.MOVE) {};

        matrix = imageView.getImageMatrix();
        matrix.getValues(matrixVals);
        hgResult = new HGResult(matrixVals[Matrix.MSCALE_X], matrixVals[Matrix.MSCALE_Y], 0, 0, 0, 0, 0);
        hgResult.setWhatGesture(HGFlags.SCALE);
        origonalWidth = imageView.getMeasuredWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight();
        origonalHeight = imageView.getMeasuredHeight() - imageView.getPaddingTop() - imageView.getPaddingBottom();;
        this.imageView = imageView;

    }//End public HGScale(ImageView imageView)


    //Top of block standard event controllers down/move/up
    public void setFirstTouch(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return;

        }

        if(matrixVals[Matrix.MSCALE_X] < 0) {

            scaleunderZeroX = true;

        }
        else {

            scaleunderZeroX = false;

        }

        if(matrixVals[Matrix.MSCALE_Y] < 0) {

            scaleunderZeroY = true;

        }
        else {

            scaleunderZeroY = false;

        }

        disableGestureInternally = false;

        int firstPointerId = 0;

        try {

            try {

                firstPointerId = event.getPointerId(0);
                firstTouchX = event.getX(firstPointerId);
                firstTouchY = event.getY(firstPointerId);

                hgResult.setFirstTouchX(firstTouchX);
                hgResult.setFirstTouchY(firstTouchY);
                hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                float[] temp = getScaledSize();
                sizeBeforeSnapActionX = temp[0];
                sizeBeforeSnapActionY = temp[1];

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setFirstTouch(MotionEvent event)


    public void setSecondTouch(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return;

        }

        int secondPointerId = 0;

        try {

            try {

                secondPointerId = event.getPointerId(1);
                secondTouchX = event.getX(secondPointerId);
                secondTouchY = event.getY(secondPointerId);
                pinchCurrentDistance += (hgResult.getTwoFingerDistance() - pinchStartDistance);
                hgResult.setSecondTouchX(secondTouchX);
                hgResult.setSecondTouchY(secondTouchY);

                hgResult.setMoveDistanceX(secondTouchX - firstTouchX);
                hgResult.setMoveDistanceY(secondTouchY - firstTouchY);
                pinchStartDistance = hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY);
                hgResult.setTwoFingerDistance(pinchStartDistance);

                //When over scaled 2 finger reduces size by 1 pixel to that on move can catch the event
                if(overOrUnderScaled == true) {

                    if(minimumSnap != 0 && maximumSnap != 0 && snapTolerance != 0) {

                        if(origonalWidth < origonalHeight) {

                            if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) >= maximumSnap)) {

                                matrixVals[Matrix.MSCALE_Y] = (maximumSnap - snapTolerance) / origonalHeight;
                                matrixVals[Matrix.MSCALE_X] = matrixVals[Matrix.MSCALE_Y] / (origonalHeight / origonalWidth);

                            }
                            else if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) <= maximumSnap)) {

                                matrixVals[Matrix.MSCALE_Y] = (minimumSnap + snapTolerance) / origonalHeight;
                                matrixVals[Matrix.MSCALE_X] = matrixVals[Matrix.MSCALE_Y] / (origonalHeight / origonalWidth);

                            }//End if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) >= maximumSnap))

                        }
                        else if(origonalWidth >= origonalHeight) {

                            if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) >= maximumSnap)) {

                                matrixVals[Matrix.MSCALE_X] = (maximumSnap - snapTolerance) / origonalWidth;
                                matrixVals[Matrix.MSCALE_Y] = matrixVals[Matrix.MSCALE_X] / (origonalWidth / origonalHeight);

                            }
                            else if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) <= maximumSnap)) {

                                matrixVals[Matrix.MSCALE_X] = (minimumSnap + snapTolerance) / origonalWidth;
                                matrixVals[Matrix.MSCALE_Y] = matrixVals[Matrix.MSCALE_X] / (origonalWidth / origonalHeight);

                            }//End if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) >= maximumSnap))

                        }//End if(origonalWidth < origonalHeight)

                    }
                    else if(minimumSnap != 0 && maximumSnap != 0 && snapTolerance == 0) {

                        if(origonalWidth < origonalHeight) {

                            if((matrixVals[Matrix.MSCALE_X] * origonalHeight) >= maximumSnap) {

                                matrixVals[Matrix.MSCALE_Y] = (maximumSnap - 1) / origonalHeight;
                                matrixVals[Matrix.MSCALE_X] = matrixVals[Matrix.MSCALE_Y] / (origonalHeight / origonalWidth);

                            }
                            else if((matrixVals[Matrix.MSCALE_X] * origonalHeight) <= minimumSnap) {

                                matrixVals[Matrix.MSCALE_Y] = (minimumSnap + 1) / origonalHeight;
                                matrixVals[Matrix.MSCALE_X] = matrixVals[Matrix.MSCALE_Y] / (origonalHeight / origonalWidth);

                            }//End if(matrixVals[Matrix.MSCALE_Y] * origonalHeight >= maximumSnap)

                        }
                        else if(origonalWidth >= origonalHeight) {

                            if((matrixVals[Matrix.MSCALE_X] * origonalWidth) >= maximumSnap) {

                                matrixVals[Matrix.MSCALE_X] = (maximumSnap - 1) / origonalWidth;
                                matrixVals[Matrix.MSCALE_Y] = matrixVals[Matrix.MSCALE_X] / (origonalWidth / origonalHeight);

                            }
                            else if((matrixVals[Matrix.MSCALE_X] * origonalWidth) <= minimumSnap) {

                                matrixVals[Matrix.MSCALE_X] = (minimumSnap + 1) / origonalWidth;
                                matrixVals[Matrix.MSCALE_Y] = matrixVals[Matrix.MSCALE_X] / (origonalWidth / origonalHeight);

                            }//End if((matrixVals[Matrix.MSCALE_X] * origonalWidth) >= maximumSnap)

                        }//End if(origonalWidth < origonalHeight)

                    }//End if(minimumSnap != 0 && maximumSnap != 0 && snapTolerance != 0)

                    manualScale(matrixVals[Matrix.MSCALE_X], matrixVals[Matrix.MSCALE_Y]);

                    overOrUnderScaled = false;

                }//End if(overOrUnderScaled == true)

                matrix = imageView.getImageMatrix();
                matrix.getValues(matrixVals);

                moveTransX = matrixVals[Matrix.MTRANS_X] + (((origonalWidth * matrixVals[Matrix.MSCALE_X]) - origonalWidth) / 2);
                moveTransY = matrixVals[Matrix.MTRANS_Y] + (((origonalHeight * matrixVals[Matrix.MSCALE_Y]) - origonalHeight) / 2);

                hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                if(snapToCenter == true) {

                    moveTransX = 0;
                    moveTransY = 0;

                }

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setSecondTouch(MotionEvent event)


    public HGResult getDown(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        return hgResult;

    }//End public HGResult getDown(MotionEvent event)


    public HGResult getMotion(MotionEvent event) {

        //Dev Note: this if block is an unclean solution. Problem was when imageView is contained in it's own layout it wasn't working on older devices. Needs fixing (not priority.)
        if(imageView.isFocused() == false) {

            (imageView.getParent()).requestLayout();

        }

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(disableGestureInternally == true) {

            return hgResult;

        }

        //Top of block to call extendible behaviour methods that may be added to this class
        if(minimumSnap != 0 || maximumSnap != 0 || snapTolerance != 0) {

            minMaxSnapIsBeingUsed = true;

            if(minimumSnap != 0 && maximumSnap != 0 && snapTolerance != 0) {

                //Does minimum and maximum size with snapping or simply snaps every amount of pixels
                scaleMinMaxWithSnapping(event);
                return hgResult;

            }
            else if(minimumSnap != 0 && maximumSnap != 0 && snapTolerance == 0) {

                //Sets minimum and maximum size allowed
                scaleMinMaxSizeMotion(event);
                return hgResult;

            }
            else if(minimumSnap == 0 && maximumSnap == 0 && snapTolerance != 0) {

                //Does minimum and maximum size with snapping or simply snaps every amount of pixels
                scaleMinMaxWithSnapping(event);
                return hgResult;

            }
            else {

                minMaxSnapIsBeingUsed = false;

            }//End if(minimumSnap != 0 && maximumSnap != 0 && snapTolerance != 0)

        }
        else {

            minMaxSnapIsBeingUsed = false;

        }//End if(minimumSnap != 0 || maximumSnap != 0 || snapTolerance != 0)
        //Bottom of block to call extendible behaviour methods that may be added to this class

        try {

            try {

                if(event.getPointerCount() > 1) {

                    firstPointerId = event.getPointerId(0);
                    secondPointerId = event.getPointerId(1);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    secondPointerIdx = event.findPointerIndex(secondPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                    secondTouchX = event.getX(secondPointerIdx);
                    secondTouchY = event.getY(secondPointerIdx);

                    if(hasbeenManuallyScaledOnDown == false) {

                        if(scaleunderZeroX == true || scaleunderZeroY == true) {

                            matrixVals[Matrix.MSCALE_X] = -((hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance);
                            matrixVals[Matrix.MSCALE_Y] = -((hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance);

                        }
                        else {

                            matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                            matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;

                        }//End if(scaleunderZeroX == true || scaleunderZeroY == true)

                    }
                    else {

                        if(origonalWidth < origonalHeight) {

                            pinchStartDistance /= ((hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / (pinchStartDistance / matrixVals[Matrix.MSCALE_X]));

                        }
                        else {

                            pinchStartDistance /= ((hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / (pinchStartDistance / matrixVals[Matrix.MSCALE_Y]));

                        }

                        hasbeenManuallyScaledOnDown = false;

                    }//End if(hasbeenManuallyScaledOnDown == false)

                    matrixVals[Matrix.MTRANS_X] = moveTransX - (((origonalWidth * matrixVals[Matrix.MSCALE_X]) - origonalWidth) / 2);
                    matrixVals[Matrix.MTRANS_Y] = moveTransY - (((origonalHeight * matrixVals[Matrix.MSCALE_Y]) - origonalHeight) / 2);

                    hgResult.setMoveDistanceX(secondTouchX - firstTouchX);
                    hgResult.setMoveDistanceY(secondTouchY - firstTouchY);

                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);
                    hgResult.setSecondTouchX(secondTouchX);
                    hgResult.setSecondTouchY(secondTouchY);
                    hgResult.setTwoFingerDistance(hgResult.getTwoFingerDistance());

                    hgResult.setScaleX(matrixVals[Matrix.MSCALE_X]);
                    hgResult.setScaleY(matrixVals[Matrix.MSCALE_Y]);
                    hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                    hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                    if(suppressInvalidaton == false) {

                        matrix.setValues(matrixVals);
                        imageView.setImageMatrix(matrix);
                        imageView.invalidate();

                    }

                    return hgResult;

                }//End  if(event.getPointerCount() > 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

        return hgResult;

    }//End public HGResult getMotion(MotionEvent event)


    public HGResult getUp(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(disableGestureInternally == true) {

            firstPointerId = event.getPointerId(0);
            firstPointerIdx = event.findPointerIndex(firstPointerId);
            firstTouchX = event.getX(firstPointerIdx);
            secondTouchY = event.getY(firstPointerIdx);
            hgResult.setFirstTouchX(firstTouchX);
            hgResult.setFirstTouchY(secondTouchY);

            return hgResult;

        }
        else {

            if(event.getPointerCount() > 1) {

                disableGestureInternally = true;

                secondPointerId = event.getPointerId(1);
                secondPointerIdx = event.findPointerIndex(secondPointerId);
                secondTouchX = event.getX(secondPointerIdx);
                secondTouchY = event.getY(secondPointerIdx);
                hgResult.setMoveDistanceX(secondTouchX - firstTouchX);
                hgResult.setMoveDistanceY(secondTouchY - firstTouchY);
                hgResult.setSecondTouchX(secondTouchX);
                hgResult.setSecondTouchY(secondTouchY);
                hgResult.setTwoFingerDistance(hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY));

                if(hasbeenManuallyScaledOnDown == false) {

                    if(scaleunderZeroX == true || scaleunderZeroY == true) {

                        matrixVals[Matrix.MSCALE_X] = -((hgResult.getTwoFingerDistance() + pinchCurrentDistance) / pinchStartDistance);
                        matrixVals[Matrix.MSCALE_Y] = -((hgResult.getTwoFingerDistance() + pinchCurrentDistance) / pinchStartDistance);

                    }
                    else {

                        matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance() + pinchCurrentDistance) / pinchStartDistance;
                        matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance() + pinchCurrentDistance) / pinchStartDistance;

                    }//End if(scaleunderZeroX == true || scaleunderZeroY == true)

                }
                else {

                    if(origonalWidth < origonalHeight) {

                        pinchStartDistance /= ((hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / (pinchStartDistance / matrixVals[Matrix.MSCALE_X]));

                    }
                    else {

                        pinchStartDistance /= ((hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / (pinchStartDistance / matrixVals[Matrix.MSCALE_Y]));

                    }

                    hasbeenManuallyScaledOnDown = false;

                }//End if(hasbeenManuallyScaledOnDown == false)

                matrixVals[Matrix.MTRANS_X] = moveTransX - (((origonalWidth * matrixVals[Matrix.MSCALE_X]) - origonalWidth) / 2);
                matrixVals[Matrix.MTRANS_Y] = moveTransY - (((origonalHeight * matrixVals[Matrix.MSCALE_Y]) - origonalHeight) / 2);

                hgResult.setScaleX(matrixVals[Matrix.MSCALE_X]);
                hgResult.setScaleY(matrixVals[Matrix.MSCALE_Y]);
                hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                if(suppressInvalidaton == false) {

                    matrix.setValues(matrixVals);
                    imageView.setImageMatrix(matrix);
                    imageView.invalidate();

                }

                float[] temp = getScaledSize();
                sizeBeforeSnapActionX = temp[0];
                sizeBeforeSnapActionY = temp[1];

                hasbeenManuallyScaledOnDown = false;

                return hgResult;

            }//End if(event.getPointerCount() > 1)

        }//End if(disableGestureInternally == true)

        return hgResult;

    }//End public HGResult getUp(MotionEvent event)
    //Bottom of block standard event controllers down/move/up


    //Top of Block behavioural methods
    //Does minimum and maximum size with snapping
    public void scaleMinMaxWithSnapping(MotionEvent event) {

        try {

            try {

                if(event.getPointerCount() > 1) {

                    firstPointerId = event.getPointerId(0);
                    secondPointerId = event.getPointerId(1);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    secondPointerIdx = event.findPointerIndex(secondPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                    secondTouchX = event.getX(secondPointerIdx);
                    secondTouchY = event.getY(secondPointerIdx);

                    hgResult.setMoveDistanceX(secondTouchX - firstTouchX);
                    hgResult.setMoveDistanceY(secondTouchY - firstTouchY);

                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);
                    hgResult.setSecondTouchX(secondTouchX);
                    hgResult.setSecondTouchY(secondTouchY);
                    hgResult.setTwoFingerDistance(hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY));

                    if(Math.abs(sizeBeforeSnapActionX - (((hgResult.getTwoFingerDistance() + pinchCurrentDistance) / pinchStartDistance) * origonalWidth)) > snapTolerance ||
                            Math.abs(sizeBeforeSnapActionY - (((hgResult.getTwoFingerDistance() + pinchCurrentDistance) / pinchStartDistance) * origonalHeight)) > snapTolerance) {

                        //Dev Note: The maximumSnap/maximumSnap boundaries are associated with the largest X or Y dimension
                        if(origonalWidth < origonalHeight) {

                            if((((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) > minimumSnap) && ((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) < maximumSnap)) ||
                                    (minimumSnap == 0 && maximumSnap == 0)) {

                                if(hasbeenManuallyScaledOnDown == false) {

                                    matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                    matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;

                                }
                                else {

                                    pinchStartDistance /= ((hgResult.getTwoFingerDistance() + pinchCurrentDistance) / (pinchStartDistance / matrixVals[Matrix.MSCALE_X]));
                                    matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                    matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                    hasbeenManuallyScaledOnDown = false;

                                }//End if(hasbeenManuallyScaledOnDown == false)

                                overOrUnderScaled = false;

                            }
                            else {

                                if(minimumSnap != 0 && maximumSnap !=0) {

                                    overOrUnderScaled = true;
                                    disableGestureInternally = true;

                                    if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) >= maximumSnap)) {

                                        matrixVals[Matrix.MSCALE_Y] = 1 / (origonalHeight / maximumSnap);
                                        matrixVals[Matrix.MSCALE_X] = matrixVals[Matrix.MSCALE_Y] / (origonalHeight / origonalWidth);

                                    }
                                    else if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) <= maximumSnap)) {

                                        matrixVals[Matrix.MSCALE_Y] = 1 / (origonalHeight / minimumSnap);
                                        matrixVals[Matrix.MSCALE_X] = matrixVals[Matrix.MSCALE_Y] / (origonalHeight / origonalWidth);

                                    }//End if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) >= maximumSnap))

                                }//End if(minimumSnap != 0 && maximumSnap !=0)

                            }//End if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / minimumSnap)  > minimumSnap)  &&  ((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / minimumSnap) < maximumSnap))

                        }
                        else if(origonalWidth >= origonalHeight) {

                            if((((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) > minimumSnap) && ((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) < maximumSnap)) ||
                                    (minimumSnap == 0 && maximumSnap == 0)) {

                                if(hasbeenManuallyScaledOnDown == false) {

                                    matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                    matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;

                                }
                                else {

                                    pinchStartDistance /= ((hgResult.getTwoFingerDistance() + pinchCurrentDistance) / (pinchStartDistance / matrixVals[Matrix.MSCALE_Y]));
                                    matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                    matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                    hasbeenManuallyScaledOnDown = false;

                                }//End if(hasbeenManuallyScaledOnDown == false)

                                overOrUnderScaled = false;

                            }
                            else {

                                if(minimumSnap != 0 && maximumSnap !=0) {

                                    overOrUnderScaled = true;
                                    disableGestureInternally = true;

                                    if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) >= maximumSnap)) {

                                        matrixVals[Matrix.MSCALE_X] = 1 / (origonalWidth / maximumSnap);
                                        matrixVals[Matrix.MSCALE_Y] = matrixVals[Matrix.MSCALE_X] / (origonalWidth / origonalHeight);

                                    }
                                    else if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) <= maximumSnap)) {

                                        matrixVals[Matrix.MSCALE_X] = 1 / (origonalWidth / minimumSnap);
                                        matrixVals[Matrix.MSCALE_Y] = matrixVals[Matrix.MSCALE_X] / (origonalWidth / origonalHeight);

                                    }//End if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) >= maximumSnap))

                                }//End if(minimumSnap != 0 && maximumSnap !=0)

                            }//End if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) > minimumSnap) && ((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) < maximumSnap))

                        }//End if(origonalWidth < origonalHeight)

                        matrixVals[Matrix.MTRANS_X] = moveTransX - (((origonalWidth * matrixVals[Matrix.MSCALE_X]) - origonalWidth) / 2);
                        matrixVals[Matrix.MTRANS_Y] = moveTransY - (((origonalHeight * matrixVals[Matrix.MSCALE_Y]) - origonalHeight) / 2);

                        hgResult.setScaleX(matrixVals[Matrix.MSCALE_X]);
                        hgResult.setScaleY(matrixVals[Matrix.MSCALE_Y]);

                        hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                        hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                        if(suppressInvalidaton == false) {

                            matrix.setValues(matrixVals);
                            imageView.setImageMatrix(matrix);
                            imageView.invalidate();

                        }


                        float[] temp = getScaledSize();
                        sizeBeforeSnapActionX = temp[0];
                        sizeBeforeSnapActionY = temp[1];

                    }//End if ....

                    hgResult.setTwoFingerDistance(hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY));

                }//End  if(event.getPointerCount() > 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void scaleMinMaxWithSnapping(MotionEvent event)


    //Sets minimum and maximum size allowed
    public void scaleMinMaxSizeMotion(MotionEvent event) {

        try {

            try {

                if(event.getPointerCount() > 1) {

                    firstPointerId = event.getPointerId(0);
                    secondPointerId = event.getPointerId(1);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    secondPointerIdx = event.findPointerIndex(secondPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                    secondTouchX = event.getX(secondPointerIdx);
                    secondTouchY = event.getY(secondPointerIdx);

                    hgResult.setMoveDistanceX(secondTouchX - firstTouchX);
                    hgResult.setMoveDistanceY(secondTouchY - firstTouchY);

                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);
                    hgResult.setSecondTouchX(secondTouchX);
                    hgResult.setSecondTouchY(secondTouchY);
                    hgResult.setTwoFingerDistance(hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY));

                    //Dev Note: The maximumSnap/maximumSnap boundaries are associated with the largest X or Y dimension
                    if(origonalWidth < origonalHeight) {

                        if((((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) > minimumSnap) && ((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) < maximumSnap))) {

                            if(hasbeenManuallyScaledOnDown == false) {

                                matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;

                            }
                            else {

                                pinchStartDistance /= ((hgResult.getTwoFingerDistance() + pinchCurrentDistance) / (pinchStartDistance / matrixVals[Matrix.MSCALE_X]));
                                matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                hasbeenManuallyScaledOnDown = false;

                            }//End if(hasbeenManuallyScaledOnDown == false)

                            overOrUnderScaled = false;

                        }
                        else {

                            if(minimumSnap != 0 && maximumSnap !=0) {

                                overOrUnderScaled = true;
                                disableGestureInternally = true;

                                if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) >= maximumSnap)) {

                                    matrixVals[Matrix.MSCALE_Y] = 1 / (origonalHeight / maximumSnap);
                                    matrixVals[Matrix.MSCALE_X] = matrixVals[Matrix.MSCALE_Y] / (origonalHeight / origonalWidth);

                                }
                                else if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) <= maximumSnap)) {

                                    matrixVals[Matrix.MSCALE_Y] = 1 / (origonalHeight / minimumSnap);
                                    matrixVals[Matrix.MSCALE_X] = matrixVals[Matrix.MSCALE_Y] / (origonalHeight / origonalWidth);

                                }//End if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) >= maximumSnap))

                            }//End if(minimumSnap != 0 && maximumSnap !=0)

                        }//End if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / minimumSnap)  > minimumSnap)  &&  ((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / minimumSnap) < maximumSnap))

                    }
                    else if(origonalWidth >= origonalHeight) {

                        if((((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) > minimumSnap) && ((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) < maximumSnap)) ||
                                (minimumSnap == 0 && maximumSnap == 0)) {

                            if(hasbeenManuallyScaledOnDown == false) {

                                matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;

                            }
                            else {

                                pinchStartDistance /= ((hgResult.getTwoFingerDistance() + pinchCurrentDistance) / (pinchStartDistance / matrixVals[Matrix.MSCALE_Y]));
                                matrixVals[Matrix.MSCALE_X] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                matrixVals[Matrix.MSCALE_Y] = (hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) + pinchCurrentDistance) / pinchStartDistance;
                                hasbeenManuallyScaledOnDown = false;

                            }//End if(hasbeenManuallyScaledOnDown == false)

                            overOrUnderScaled = false;

                        }
                        else {

                            if(minimumSnap != 0 && maximumSnap !=0) {

                                overOrUnderScaled = true;
                                disableGestureInternally = true;

                                if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) >= maximumSnap)) {

                                    matrixVals[Matrix.MSCALE_X] = 1 / (origonalWidth / maximumSnap);
                                    matrixVals[Matrix.MSCALE_Y] = matrixVals[Matrix.MSCALE_X] / (origonalWidth / origonalHeight);

                                }
                                else if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) <= maximumSnap)) {

                                    matrixVals[Matrix.MSCALE_X] = 1 / (origonalWidth / minimumSnap);
                                    matrixVals[Matrix.MSCALE_Y] = matrixVals[Matrix.MSCALE_X] / (origonalWidth / origonalHeight);

                                }//End if(((matrixVals[Matrix.MSCALE_Y] * origonalHeight) / (origonalHeight / origonalWidth) >= maximumSnap))

                            }//End if(minimumSnap != 0 && maximumSnap !=0)

                        }//End if(((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) > minimumSnap) && ((matrixVals[Matrix.MSCALE_X] * origonalWidth) / (origonalWidth / origonalHeight) < maximumSnap))

                    }//End if(origonalWidth < origonalHeight)

                    matrixVals[Matrix.MTRANS_X] = moveTransX - (((origonalWidth * matrixVals[Matrix.MSCALE_X]) - origonalWidth) / 2);
                    matrixVals[Matrix.MTRANS_Y] = moveTransY - (((origonalHeight * matrixVals[Matrix.MSCALE_Y]) - origonalHeight) / 2);

                    hgResult.setScaleX(matrixVals[Matrix.MSCALE_X]);
                    hgResult.setScaleY(matrixVals[Matrix.MSCALE_Y]);

                    hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                    hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                    if(suppressInvalidaton == false) {

                        matrix.setValues(matrixVals);
                        imageView.setImageMatrix(matrix);
                        imageView.invalidate();

                    }


                    float[] temp = getScaledSize();
                    sizeBeforeSnapActionX = temp[0];
                    sizeBeforeSnapActionY = temp[1];

                    hgResult.setTwoFingerDistance(hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY));

                }//End  if(event.getPointerCount() > 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void scaleMinMaxSizeMotion(MotionEvent event)


    public void setMinMaxSnap(float minimumSnap, float maximumSnap, float snapTolerance) {

        this.minimumSnap = minimumSnap;
        this.maximumSnap = maximumSnap;
        this.snapTolerance = snapTolerance;

    }//End public void setMinMaxSnap(float minimumSnap, float maximumSnap, float snapTolerance)


    public float[] getMinMaxSnap() {

        float[] returnVal = new float[3];
        returnVal[0] = this.minimumSnap;
        returnVal[1] = this.maximumSnap;
        returnVal[2] = this.snapTolerance;
        return returnVal;

    }


    public void setSnapToCenter(boolean snapToCenter) {

        this.snapToCenter = snapToCenter;

    }


    public boolean getSnapToCenter() {

        return snapToCenter;

    }
    //Bottom of Block behavioural


    //Top of Block convenience methods
    public float[] getScaledSize() {

        float[] returnVal = new float[2];

        returnVal[0] = origonalWidth * matrixVals[Matrix.MSCALE_X];
        returnVal[1] = origonalHeight * matrixVals[Matrix.MSCALE_Y];

        return returnVal;

    }


    public void manualScale(float scaleX, float scaleY) {

        hasbeenManuallyScaledOnDown = true;

        matrixVals[Matrix.MSCALE_X] = scaleX;
        matrixVals[Matrix.MSCALE_Y] = scaleY;
        matrixVals[Matrix.MTRANS_X] = moveTransX - (((origonalWidth * matrixVals[Matrix.MSCALE_X]) - origonalWidth) / 2);
        matrixVals[Matrix.MTRANS_Y] = moveTransY - (((origonalHeight * matrixVals[Matrix.MSCALE_Y]) - origonalHeight) / 2);
        pinchCurrentDistance = (hgResult.getTwoFingerDistance() - pinchStartDistance);
        pinchStartDistance = hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY);
        hgResult.setTwoFingerDistance(pinchStartDistance);
        hgResult.setScaleX(scaleX);
        hgResult.setScaleY(scaleY);
        hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
        hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

        if(suppressInvalidaton == false) {

            matrix = imageView.getImageMatrix();
            matrix.setValues(matrixVals);
            imageView.setImageMatrix(matrix);
            imageView.invalidate();

        }

        float[] temp = getScaledSize();
        sizeBeforeSnapActionX = temp[0];
        sizeBeforeSnapActionY = temp[1];

    }//End public void manualScale(float scaleX, float scaleY)


    //Convenience method. Set XorY to "X" to adjust height or "Y" to adjust width.
    public Bitmap getProportionalBitmap(Bitmap bitmap, int newDimensionXorY, String XorY) {

        if (bitmap == null) {

            return null;
        }

        float xyRatio = 0;
        int newWidth = 0;
        int newHeight = 0;

        if(XorY.toLowerCase().equals("x")) {

            xyRatio = (float) newDimensionXorY / bitmap.getWidth();
            newHeight = (int) (bitmap.getHeight() * xyRatio);
            bitmap = Bitmap.createScaledBitmap(bitmap, newDimensionXorY, newHeight, true);

        }
        else if(XorY.toLowerCase().equals("y")) {

            xyRatio = (float) newDimensionXorY / bitmap.getHeight();
            newWidth = (int) (bitmap.getWidth() * xyRatio);
            bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newDimensionXorY, true);

        }

        return bitmap;

    }//End public Bitmap getProportionalBitmap(Bitmap bitmap, int newDimensionXorY, String XorY)


    public int[] getRotatedOrScaledSize(ImageView imageView) {

        int[] scaledSize = new int[2];

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        scaledSize[0] = bitmap.getWidth();
        scaledSize[1] = bitmap.getHeight();

        return scaledSize;

    }//End public int[] getRotatedOrScaledSize(ImageView imageView)
    //Bottom of Block convenience methods


    //Top of Block Common Functions
    public Bitmap getBitmapFromImageView(ImageView imageView) {

        return ((BitmapDrawable) imageView.getDrawable()).getBitmap();

    }


    public void setDisableGesture(boolean disableGestureDynamically) {

        this.disableGestureDynamically = disableGestureDynamically;

    }


    public boolean isGesturedisabled() {

        return disableGestureDynamically;

    }


    public void suppressInvalidaton(boolean suppressInvalidaton) {

        this.suppressInvalidaton = suppressInvalidaton;

    }


    public boolean getSuppressInvalidaton() {

        return suppressInvalidaton;

    }


    public float[] getMatrixVals() {

        matrix.getValues(matrixVals);
        return matrixVals;

    }


    public void setMatrixVals(float[] matrixVals) {

        this.matrixVals = matrixVals;

        moveTransX = matrixVals[Matrix.MTRANS_X] - ((origonalWidth - (origonalWidth * matrixVals[Matrix.MSCALE_X])) / 2);
        moveTransY = matrixVals[Matrix.MTRANS_Y] - ((origonalHeight - (origonalHeight * matrixVals[Matrix.MSCALE_Y])) / 2);

    }


    public void onDown(HGResult hgResult) {

        hacerGestoDown = null;

    }


    public void onMotion(HGResult hgResult) {

        hacerGestoMove = null;

    }


    public void onRelease(HGResult hgResult) {

        hacerGestoUp = null;

    }


    public HGResult getHgResult() {

        return this.hgResult;

    }


    public void expandPaddingToFitContainer(final boolean expandPadding) {

        paddingIsExpanded = expandPadding;

        origonalWidth = imageView.getMeasuredWidth();
        origonalHeight = imageView.getMeasuredHeight();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {

                                if (expandPadding == true) {

                                    paddingL = imageView.getLeft();
                                    paddingT = imageView.getTop();
                                    paddingR = ((ViewGroup) imageView.getParent()).getMeasuredWidth() - imageView.getRight();
                                    paddingB = ((ViewGroup) imageView.getParent()).getMeasuredHeight() - imageView.getBottom();
                                    imageView.setPadding(paddingL, paddingT, paddingR, paddingB);

                                }
                                else {

                                    paddingL = imageView.getPaddingLeft();
                                    paddingT = imageView.getPaddingTop();
                                    paddingR = imageView.getPaddingRight();
                                    paddingB = imageView.getPaddingBottom();
                                    imageView.setPadding(0, 0, 0, 0);

                                }//End if(expandPadding == true)

                                (imageView.getParent()).requestLayout();

                            }

                        }, 10);

            }

        });

    }//End public void expandPaddingToFitContainer(final boolean expandPadding)
    //Bottom of Block Common Functions

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        final int action = event.getAction() & MotionEvent.ACTION_MASK;

        switch(action) {

            case MotionEvent.ACTION_DOWN: {

                setFirstTouch(event);

                if(hacerGestoDown != null) {

                    iHacerGestoDown.onDown(getDown(event));

                }
                else {

                    getDown(event);

                }

                break;

            }
            case MotionEvent.ACTION_POINTER_DOWN: {

                setSecondTouch(event);

                if(hacerGestoDown != null) {

                    iHacerGestoDown.onDown(getDown(event));

                }
                else {

                    getDown(event);

                }//End if(hacerGestoDown != null)

                break;

            }
            case MotionEvent.ACTION_MOVE: {

                if(hacerGestoMove != null) {

                    iHacerGestoMove.onMotion(getMotion(event));

                }

                break;

            }
            case MotionEvent.ACTION_POINTER_UP: {

                if(hacerGestoUp != null) {

                    iHacerGestoUp.onRelease(getUp(event));

                }
                else {

                    getUp(event);

                }//End if(hacerGestoUp != null)

                break;

            }
            case MotionEvent.ACTION_UP: {

                if(hacerGestoUp != null) {

                    iHacerGestoUp.onRelease(getUp(event));

                }
                else {

                    getUp(event);

                }

                break;

            }
            default:

                break;

        }//End switch(action)

        return true;

    }//End public boolean onTouch(View view, MotionEvent event)

}