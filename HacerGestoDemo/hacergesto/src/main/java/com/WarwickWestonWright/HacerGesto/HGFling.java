/*
Notes about this class: The encapsulation between this class and the other gesture classes is absolute with the exception of the setMatrixVals(float[] matrixVals) method.
When extending this class please refer the the *Extensibility notes:* in the overview section of the repo at: https://bitbucket.org/warwick/hacergestov1
Examining how this function 'triggerFling(dynamicFlingDistance, event.getRawX() - firstTouchX, event.getRawY() - firstTouchY)' will give you a clear idea of how this
class was designed to be extended.
For the sake of keeping the code clean, methods should be added at the block starting with the comment '//Top of Block behavioural methods'
*/
package com.WarwickWestonWright.HacerGesto;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public abstract class HGFling implements View.OnTouchListener, HacerGestoDown.IHacerGestoDown, HacerGestoMove.IHacerGestoMove, HacerGestoUp.IHacerGestoUp {

    private static boolean disableGestureInternally = false;
    private static boolean disableGestureDynamically = false;

    private ImageView imageView = null;
    private HGResult hgResult = null;
    private Matrix matrix = null;
    private static Point point;
    private static float[] matrixVals = null;
    private static int measureSpec = 0;

    private static boolean paddingIsExpanded = false;
    private static boolean bounceBack = false;
    private static int dynamicFlingDistance = 0;

    private static int firstPointerId = 0;
    private static int firstPointerIdx = 0;
    private static int secondPointerId = 0;
    private static int secondPointerIdx = 0;

    private static int origonalWidth;
    private static int origonalHeight;
    private static int paddingL;
    private static int paddingT;
    private static int paddingR;
    private static int paddingB;
    private static int leftRelativeToPadding;
    private static int topRelativeToPadding;
    private static int rightRelativeToPadding;
    private static int bottomRelativeToPadding;

    private static float firstTouchX;
    private static float firstTouchY;
    private static float secondTouchX;
    private static float secondTouchY;

    private static float widthToHEightFlingRatio;
    private static int flingDistanceThreashold;
    private static int flingTimeThreashold;
    private static int flingAnimationTime;
    private static long gestureStartTime;

    private TranslateAnimation translateAnimation;

    private static HacerGestoDown.IHacerGestoDown iHacerGestoDown = null;
    private static HacerGestoMove.IHacerGestoMove iHacerGestoMove = null;
    private static HacerGestoUp.IHacerGestoUp iHacerGestoUp = null;

    private static HacerGestoDown hacerGestoDown = null;
    private static HacerGestoMove hacerGestoMove = null;
    private static HacerGestoUp hacerGestoUp = null;

    public HGFling(ImageView imageView) {

        disableGestureInternally = false;
        disableGestureDynamically = false;
        paddingIsExpanded = false;
        bounceBack = false;
        dynamicFlingDistance = 0;

        matrixVals = new float[9];
        origonalWidth = 0;
        origonalHeight = 0;
        paddingL = imageView.getPaddingLeft();
        paddingT = imageView.getPaddingTop();
        paddingR = imageView.getPaddingRight();
        paddingB = imageView.getPaddingBottom();
        leftRelativeToPadding = 0;
        topRelativeToPadding = 0;
        rightRelativeToPadding = 0;
        bottomRelativeToPadding = 0;

        firstTouchX = 0;
        firstTouchY = 0;

        widthToHEightFlingRatio = 0;
        flingDistanceThreashold = 0;
        flingTimeThreashold = 280;
        flingAnimationTime = 300;
        gestureStartTime = 0;

        measureSpec = View.MeasureSpec.getSize(View.MeasureSpec.UNSPECIFIED);
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        imageView.measure(measureSpec, measureSpec);

        iHacerGestoDown = this;
        iHacerGestoMove = this;
        iHacerGestoUp = this;

        hacerGestoDown = new HacerGestoDown(imageView, HGFlags.MOVE) {};
        hacerGestoMove = new HacerGestoMove(imageView, HGFlags.MOVE) {};
        hacerGestoUp = new HacerGestoUp(imageView, HGFlags.MOVE) {};

        matrix = imageView.getImageMatrix();
        matrix.getValues(matrixVals);
        hgResult = new HGResult(matrixVals[Matrix.MSCALE_X], matrixVals[Matrix.MSCALE_Y], 0, 0, 0, 0, 0);
        hgResult.setWhatGesture(HGFlags.FLING);

        point = new Point(imageView.getResources().getDisplayMetrics().widthPixels, imageView.getResources().getDisplayMetrics().heightPixels);
        flingDistanceThreashold = ((point.x < point.y) ? point.x : point.y) / 3;//Default fling distance to 1/4 of screen width.

        origonalWidth = imageView.getMeasuredWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight();
        origonalHeight = imageView.getMeasuredHeight() - imageView.getPaddingTop() - imageView.getPaddingBottom();;

        this.imageView = imageView;

    }//End public HGMove(ImageView imageView)


    //Top of block standard event controllers down/move/up
    public void setFirstTouch(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return;

        }

        disableGestureInternally = false;
        firstPointerId = 0;
        firstPointerIdx = 0;
        secondPointerId = 0;
        secondPointerIdx = 0;
        gestureStartTime = 0;

        hgResult.setMoveDistanceX(0);
        hgResult.setMoveDistanceY(0);

        try {

            try {

                if(event.getPointerCount() == 1) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getRawX();
                    firstTouchY = event.getRawY();
                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);
                    secondTouchX = 0;
                    secondTouchY = 0;
                    gestureStartTime = System.currentTimeMillis();

                }//End if(event.getPointerCount() == 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setFirstTouch(MotionEvent event)


    public void setSecondTouch(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return;

        }

        disableGestureInternally = true;

        try {

            try {

                secondPointerId = event.getPointerId(1);
                secondPointerIdx = event.findPointerIndex(secondPointerId);
                secondTouchX = event.getRawX();
                secondTouchY = event.getRawY();
                hgResult.setSecondTouchX(secondTouchX);
                hgResult.setSecondTouchY(secondTouchY);

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setSecondTouch(MotionEvent event)


    public HGResult getDown(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(disableGestureInternally == false) {

            gestureStartTime = System.currentTimeMillis();

        }

        return hgResult;

    }//End public HGResult getDown(MotionEvent event)


    public HGResult getMotion(MotionEvent event) {

        //Dev Note: this if block is an unclean solution. Problem was when imageView is contained in it's own layout it wasn't working on older devices. Needs fixing (not priority.)
        if(imageView.isFocused() == false) {

            (imageView.getParent()).requestLayout();

        }

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(event.getPointerCount() > 1) {

            disableGestureInternally = true;

        }

        return hgResult;

    }


    public HGResult getUp(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(disableGestureInternally == true) {

            return hgResult;

        }

        //Check flingTimeThreashold
        if(System.currentTimeMillis() < gestureStartTime + flingTimeThreashold) {

            hgResult.setTwoFingerDistance(hgResult.getTwoFingerDistance(firstTouchX, firstTouchY, event.getRawX(), event.getRawY()));
            //Check flingDistanceThreashold
            if(hgResult.getTwoFingerDistance() > flingDistanceThreashold) {

                paddingL = imageView.getPaddingLeft();
                paddingT = imageView.getPaddingTop();
                paddingR = imageView.getPaddingRight();
                paddingB = imageView.getPaddingBottom();

                leftRelativeToPadding = imageView.getLeft() + paddingL;
                topRelativeToPadding = imageView.getTop() + paddingT;
                rightRelativeToPadding = imageView.getMeasuredWidth() - paddingR;
                bottomRelativeToPadding = imageView.getMeasuredHeight() - paddingB;

                widthToHEightFlingRatio = Math.abs(firstTouchY - event.getRawY()) / Math.abs(firstTouchX - event.getRawX());

                float animationStartX = matrixVals[Matrix.MTRANS_X];
                float animationEndX = 0;
                float animationStartY = matrixVals[Matrix.MTRANS_Y];
                float animationEndY = 0;

                if(dynamicFlingDistance == 0) {//This block is for default fling behaviour flings to the nearest edge.

                    if(firstTouchX < event.getRawX()) {//Fling to right

                        if(firstTouchY < event.getRawY()) {//Fling right & down

                            if((point.x - leftRelativeToPadding - imageView.getLeft()) * widthToHEightFlingRatio < point.x) {

                                //Right edge is closest
                                animationEndX = imageView.getMeasuredWidth() - paddingL;
                                animationEndY = (int) ((imageView.getBottom() - topRelativeToPadding) * widthToHEightFlingRatio);

                            }
                            else {

                                //Bottom edge is closest
                                animationEndX = (int) ((imageView.getRight() - leftRelativeToPadding) / widthToHEightFlingRatio);
                                animationEndY = imageView.getMeasuredHeight() - paddingT;

                            }//End if((point.x - leftRelativeToPadding - imageView.getLeft()) * widthToHEightFlingRatio < point.x)

                        }
                        else /*if(firstTouchY > releaseY)*/ {//Fling right & up

                            if((point.x - leftRelativeToPadding - imageView.getLeft()) * widthToHEightFlingRatio < point.x) {

                                //Right edge is closest
                                animationEndX = imageView.getMeasuredWidth() - paddingL;
                                animationEndY = (int) -((imageView.getBottom() - topRelativeToPadding) * widthToHEightFlingRatio);

                            }
                            else {

                                //Top edge is closest
                                animationEndX = (int) ((imageView.getRight() - leftRelativeToPadding) / widthToHEightFlingRatio);
                                animationEndY = -(imageView.getMeasuredHeight() - paddingT);

                            }//End if((point.x - leftRelativeToPadding - imageView.getLeft()) * widthToHEightFlingRatio < point.x)

                        }//End if(firstTouchY < event.getRawY())

                    }
                    else /*if(firstTouchX > releaseX)*/ {//Fling to left

                        if(firstTouchY < event.getRawY()) {//Fling left down

                            if((point.x - leftRelativeToPadding - imageView.getLeft()) * widthToHEightFlingRatio < point.x) {

                                //Left edge is closest
                                animationEndX = -(imageView.getMeasuredWidth() - paddingL);
                                animationEndY = (int) ((imageView.getBottom() - topRelativeToPadding) * widthToHEightFlingRatio);

                            }
                            else {

                                //Bottom edge is closest
                                animationEndX = (int) - ((imageView.getRight() - leftRelativeToPadding) / widthToHEightFlingRatio);
                                animationEndY = imageView.getMeasuredHeight() - paddingT;

                            }//End if((point.x - leftRelativeToPadding - imageView.getLeft()) * widthToHEightFlingRatio < point.x)

                        }
                        else /*if(firstTouchY > releaseY)*/ {//Fling left up

                            if((point.x - leftRelativeToPadding - imageView.getLeft()) * widthToHEightFlingRatio < point.x) {

                                //Left edge is closest
                                animationEndX = -(imageView.getMeasuredWidth() - paddingL);
                                animationEndY = (int) -((imageView.getBottom() - topRelativeToPadding) * widthToHEightFlingRatio);

                            }
                            else {

                                //Top edge is closest
                                animationEndX = (int) - ((imageView.getRight() - leftRelativeToPadding) / widthToHEightFlingRatio);
                                animationEndY = -(imageView.getMeasuredHeight() - paddingT);

                            }//End if((point.x - leftRelativeToPadding - imageView.getLeft()) * widthToHEightFlingRatio < point.x)

                        }//End if(firstTouchY < event.getRawY())

                    }//End if(firstTouchX < event.getRawX())

                    translateAnimation = new TranslateAnimation(animationStartX, animationEndX, animationStartY, animationEndY);
                    imageView.setAnimation(translateAnimation);
                    imageView.getAnimation().setDuration(flingAnimationTime);
                    imageView.startAnimation(translateAnimation);

                    if(bounceBack == false) {

                        final float animationEndXFinal = animationEndX;
                        final float animationEndYFinal = animationEndY;

                        hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                        hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                        new Handler().postDelayed(
                                new Runnable() {
                                    @Override
                                    public void run() {

                                        matrixVals[Matrix.MTRANS_X] = animationEndXFinal;
                                        matrixVals[Matrix.MTRANS_Y] = animationEndYFinal;

                                        matrix.setValues(matrixVals);
                                        imageView.setImageMatrix(matrix);
                                        imageView.invalidate();
                                        imageView.requestLayout();

                                    }
                                }, flingAnimationTime);

                    }//End if(bounceBack == false)

                }
                else /*if(dynamicFlingDistance != 0)*/ {//condition that is called when setFlingDistance(int dynamicFlingDistance) method sets a non zero value

                    triggerFling(dynamicFlingDistance, event.getRawX() - firstTouchX, event.getRawY() - firstTouchY);

                }//End if(dynamicFlingDistance == 0)

            }//End if(hgResult.getTwoFingerDistance() > flingDistanceThreashold)

        }//End if(System.currentTimeMillis() < gestureStartTime + flingTimeThreashold)

        hgResult.setFirstTouchX(event.getRawX());
        hgResult.setFirstTouchY(event.getRawY());
        hgResult.setMoveDistanceX(event.getRawX() - firstTouchX);
        hgResult.setMoveDistanceY(event.getRawY() - firstTouchY);

        return hgResult;

    }//End HGResult getUp(MotionEvent event)
    //Bottom of block standard event controllers down/move/up


    //Top of Block behavioural methods
    public void triggerFling(float dynamicFlingDistance, float width, float height) {

        float animationStartX = (int) matrixVals[Matrix.MTRANS_X];
        float animationStartY = (int) matrixVals[Matrix.MTRANS_X];
        float animationEndX = 0;
        float animationEndY = 0;

        if(dynamicFlingDistance == 0) {

            animationEndX = width;
            animationEndY = height;

        }
        else {

            if(width == 0 || height == 0) {

                if(width == 0 && height !=0) {

                    animationEndX = (int) matrixVals[Matrix.MTRANS_X];
                    animationEndY = height;

                }
                else if(width != 0 && height ==0) {

                    animationEndX = width;
                    animationEndY = (int) matrixVals[Matrix.MTRANS_X];

                }
                else if(width == 0 && height == 0) {return;}//End if(width == 0 && height !=0)

            }
            else {

                if(height > 0) {

                    animationEndY = (float) Math.sqrt((dynamicFlingDistance * dynamicFlingDistance) / (Math.pow((width / height), 2) + Math.pow((height / height), 2)));
                    animationEndX = (animationEndY / (height / width));

                }
                else {

                    animationEndY = (float) -Math.sqrt((dynamicFlingDistance * dynamicFlingDistance) / (Math.pow((width / height), 2) + Math.pow((height / height), 2)));
                    animationEndX = (animationEndY / (height / width));

                }

            }//End if(width ==0 || height == 0)

        }//End if(dynamicFlingDistance == 0)

        translateAnimation = new TranslateAnimation(animationStartX, animationEndX, animationStartY, animationEndY);
        translateAnimation.setFillEnabled(true);
        imageView.setAnimation(translateAnimation);
        imageView.getAnimation().setDuration(flingAnimationTime);
        imageView.startAnimation(translateAnimation);

        if(bounceBack == false) {

            final float animationEndXFinal = animationEndX;
            final float animationEndYFinal = animationEndY;

            hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
            hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {

                            matrixVals[Matrix.MTRANS_X] = animationEndXFinal;
                            matrixVals[Matrix.MTRANS_Y] = animationEndYFinal;

                            matrix.setValues(matrixVals);
                            imageView.setImageMatrix(matrix);
                            imageView.invalidate();
                            imageView.requestLayout();

                        }
                    }, flingAnimationTime);

        }//End if(bounceBack == false)

    }//End public void triggerFling(int dynamicFlingDistance, float widthToHEightFlingRatio, MotionEvent event)


    public void setupFlingValues(int flingDistanceThreashold, int flingTimeThreashold, int flingAnimationTime) {

        this.flingDistanceThreashold = flingDistanceThreashold;
        this.flingTimeThreashold = flingTimeThreashold;
        this.flingAnimationTime = flingAnimationTime;

    }


    public void setFlingDistance(int dynamicFlingDistance) {

        this.dynamicFlingDistance = dynamicFlingDistance;

    }


    public int getFlingDistance() {

        return this.dynamicFlingDistance;

    }


    public void setFlingBounceBack(boolean bounceBack) {

        this.bounceBack = bounceBack;

    }


    public boolean getBounceBack() {

        return bounceBack;

    }
    //Bottom of Block behavioural methods


    //Top of Block Common Functions
    public void setMatrixVals(float[] matrixVals) {

        this.matrixVals = matrixVals;
        this.matrixVals[Matrix.MTRANS_X] = matrixVals[Matrix.MTRANS_X];
        this.matrixVals[Matrix.MTRANS_Y] = matrixVals[Matrix.MTRANS_Y];

    }


    public float[] getMatrixVals() {

        matrix.getValues(matrixVals);
        return matrixVals;

    }


    public void setDisableGesture(boolean disableGestureDynamically) {

        this.disableGestureDynamically = disableGestureDynamically;

    }


    public boolean isGesturedisabled() {

        return disableGestureDynamically;

    }


    public Bitmap getBitmapFromImageView(ImageView imageView) {

        return ((BitmapDrawable) imageView.getDrawable()).getBitmap();

    }


    public void onDown(HGResult hgResult) {

        hacerGestoDown = null;

    }


    public void onMotion(HGResult hgResult) {

        hacerGestoMove = null;

    }


    public void onRelease(HGResult hgResult) {

        hacerGestoUp = null;

    }


    public HGResult getHgResult() {

        return this.hgResult;

    }


    public void expandPaddingToFitContainer(final boolean expandPadding) {

        paddingIsExpanded = expandPadding;

        origonalWidth = imageView.getMeasuredWidth();
        origonalHeight = imageView.getMeasuredHeight();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {

                                if (expandPadding == true) {

                                    paddingL = imageView.getLeft();
                                    paddingT = imageView.getTop();
                                    paddingR = ((ViewGroup) imageView.getParent()).getMeasuredWidth() - imageView.getRight();
                                    paddingB = ((ViewGroup) imageView.getParent()).getMeasuredHeight() - imageView.getBottom();
                                    imageView.setPadding(paddingL, paddingT, paddingR, paddingB);

                                }
                                else {

                                    paddingL = imageView.getPaddingLeft();
                                    paddingT = imageView.getPaddingTop();
                                    paddingR = imageView.getPaddingRight();
                                    paddingB = imageView.getPaddingBottom();
                                    imageView.setPadding(0, 0, 0, 0);

                                }//End if(expandPadding == true)

                                (imageView.getParent()).requestLayout();

                            }

                        }, 10);

            }

        });

    }//End public void expandPaddingToFitContainer(final boolean expandPadding)
    //Bottom of Block Common Functions


    @Override
    public boolean onTouch(View view, MotionEvent event) {

        final int action = event.getAction() & MotionEvent.ACTION_MASK;

        switch(action) {

            case MotionEvent.ACTION_DOWN: {

                if(hacerGestoDown != null) {

                    setFirstTouch(event);
                    iHacerGestoDown.onDown(getDown(event));

                }
                else {

                    setFirstTouch(event);

                }

                break;

            }
            case MotionEvent.ACTION_POINTER_DOWN: {

                if(hacerGestoDown != null) {

                    setSecondTouch(event);
                    iHacerGestoDown.onDown(getDown(event));

                }
                else {

                    setSecondTouch(event);

                }

                break;

            }
            case MotionEvent.ACTION_MOVE: {

                if(hacerGestoMove != null) {

                    iHacerGestoMove.onMotion(getMotion(event));

                }
                else {

                    getMotion(event);

                }

                break;

            }
            case MotionEvent.ACTION_POINTER_UP: {

                //Not used in this context

                break;

            }
            case MotionEvent.ACTION_UP: {

                if(hacerGestoUp != null) {

                    iHacerGestoUp.onRelease(getUp(event));

                }
                else {

                    getUp(event);

                }

                break;

            }
            default:

                break;

        }//End switch(action)

        return true;

    }//End public boolean onTouch(View view, MotionEvent event)

}