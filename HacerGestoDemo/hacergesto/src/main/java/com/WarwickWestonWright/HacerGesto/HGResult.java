package com.WarwickWestonWright.HacerGesto;

public class HGResult {

    private int whatGesture = 0;
    private float xPos = 0;
    private float yPos = 0;
    private float scaleX;
    private float scaleY;
    private float angle;
    private float firstTouchX = 0;
    private float firstTouchY = 0;
    private float secondTouchX = 0;
    private float secondTouchY = 0;
    private float moveDistanceX = 0;
    private float moveDistanceY = 0;
    private float twoFingerDistance;

    //Default Constructor
    public HGResult() {

        this.whatGesture = 0;
        this.xPos = 0;
        this.yPos = 0;
        this.scaleX = 1;
        this.scaleY = 1;
        this.angle = 0;
        this.firstTouchX = 0;
        this.firstTouchY = 0;
        this.secondTouchX = 0;
        this.secondTouchY = 0;
        this.moveDistanceX = 0;
        this.moveDistanceY = 0;
        this.twoFingerDistance = 0;

    }

    public HGResult(float scaleX, float scaleY, float angle, float firstTouchX, float firstTouchY, float secondTouchX, float secondTouchY) {

        this.whatGesture = 0;
        this.xPos = 0;
        this.yPos = 0;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.angle = angle;
        this.firstTouchX = firstTouchX;
        this.firstTouchY = firstTouchY;
        this.secondTouchX = secondTouchX;
        this.secondTouchY = secondTouchY;
        this.moveDistanceX = 0;
        this.moveDistanceY = 0;
        this.twoFingerDistance = 0;

    }

    /*Accessors*/
    public int getWhatGesture() {return this.whatGesture;}

    public float getXPos() {return this.xPos;}

    public float getYPos() {return this.yPos;}

    public float getScaleX() {return this.scaleX;}

    public float getScaleY() {return this.scaleY;}

    public float getAngle() {return this.angle;}

    public float getFirstTouchX() {return this.firstTouchX;}

    public float getFirstTouchY() {return this.firstTouchY;}

    public float getSecondTouchX() {return this.secondTouchX;}

    public float getSecondTouchY() {return this.secondTouchY;}

    public float getMoveDistanceX() {return this.moveDistanceX;}

    public float getMoveDistanceY() {return this.moveDistanceY;}

    public float getTwoFingerDistance() {return this.twoFingerDistance;}

    public float getTwoFingerDistance(float firstTouchX, float firstTouchY, float secondTouchX, float secondTouchY) {

        float pinchDistanceX = Math.abs(firstTouchX - secondTouchX);
        float pinchDistanceY = Math.abs(firstTouchY - secondTouchY);

        /* Pinch Distance algorithm is not complete */
        if(pinchDistanceX == 0 && pinchDistanceY == 0) {

            return  0;

        }
        else {

            pinchDistanceX = (pinchDistanceX * pinchDistanceX);
            pinchDistanceY = (pinchDistanceY * pinchDistanceY);
            return (float) Math.sqrt(pinchDistanceX + pinchDistanceY);

        }//End if(pinchDistanceX == 0 && pinchDistanceY == 0)

    }//End private float getTwoFingerDistance(int firstTouchX, int firstTouchY, int secondTouchX, int secondTouchY)

    /*Mutators*/
    public void setWhatGesture(int whatGesture) {this.whatGesture = whatGesture;}

    public void setXPos(float xPos) {this.xPos = xPos;}

    public void setYPos(float yPos) {this.yPos = yPos;}

    public void setScaleX(float scaleX) {this.scaleX = scaleX;}

    public void setScaleY(float scaleY) {this.scaleY = scaleY;}

    public void setAngle(float angle) {this.angle = angle;}

    public void setFirstTouchX(float firstTouchX) {this.firstTouchX = firstTouchX;}

    public void setFirstTouchY(float firstTouchY) {this.firstTouchY = firstTouchY;}

    public void setSecondTouchX(float secondTouchX) {this.secondTouchX = secondTouchX;}

    public void setSecondTouchY(float secondTouchY) {this.secondTouchY = secondTouchY;}

    public void setMoveDistanceX(float moveDistanceX) {this.moveDistanceX = moveDistanceX;}

    public void setMoveDistanceY(float moveDistanceY) {this.moveDistanceY = moveDistanceY;}

    public void setTwoFingerDistance(float twoFingerDistance) {this.twoFingerDistance = twoFingerDistance;}

}