/*
Notes about this class: The encapsulation between this class and the other gesture classes is absolute with the exception of the setMatrixVals(float[] matrixVals) method.
When extending this class please refer the the *Extensibility notes:* in the overview section of the repo at: https://bitbucket.org/warwick/hacergestov1
There is a section in this class with a comment block '//Top of block to call extendible behaviour methods that may be added to this class'
It is in this section that I (the developer) recommend extensible methods should be called. There is a function here 'angleSnapMotion(event)'. Examining how this function
is called will give you a clear idea of how this class was designed to be extended.
For the sake of keeping the code clean, methods should be added at the block starting with the comment '//Top of Block behavioural methods'
*/
package com.WarwickWestonWright.HacerGesto;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public abstract class HGRotate implements View.OnTouchListener, HacerGestoDown.IHacerGestoDown, HacerGestoMove.IHacerGestoMove, HacerGestoUp.IHacerGestoUp {

    private static boolean disableGestureInternally = false;
    private static boolean disableGestureDynamically = false;
    private static boolean dialMode = false;
    private static boolean suppressInvalidaton = false;
    private static float angleSnap = 0;
    private static float angleSnapTemp = 0;
    private static float angleSnapAngleDifference = 0;
    private static float angleSnapToleranceTemp = 0;
    private static float angleSnapTolerance = 0;

    private ImageView imageView = null;
    private HGResult hgResult = null;
    private Matrix matrix = null;
    private static float[] matrixVals = null;
    private static int measureSpec = 0;
    private static boolean snapToCenter = false;
    private static boolean paddingIsExpanded = false;

    private static float angle = 0;
    private static float angleDifference = 0;
    private static float cumulativeAngle = 0;
    private static Point secondPoint;
    private static Point gestureCenterPoint;
    private static Point rotationCenterPoint;

    private static int firstPointerId = 0;
    private static int secondPointerId = 0;
    private static int firstPointerIdx = 0;
    private static int secondPointerIdx = 0;

    private static int origonalWidth = 0;
    private static int origonalHeight = 0;
    private static int paddingL = 0;
    private static int paddingT = 0;
    private static int paddingR = 0;
    private static int paddingB = 0;

    private static float firstTouchX = 0;
    private static float firstTouchY = 0;
    private static float secondTouchX = 0;
    private static float secondTouchY = 0;

    private static int[] scaledSize = null;

    private HacerGestoDown.IHacerGestoDown iHacerGestoDown = null;
    private HacerGestoMove.IHacerGestoMove iHacerGestoMove = null;
    private HacerGestoUp.IHacerGestoUp iHacerGestoUp = null;

    private static HacerGestoDown hacerGestoDown = null;
    private static HacerGestoMove hacerGestoMove = null;
    private static HacerGestoUp hacerGestoUp = null;

    public HGRotate(ImageView imageView) {

        disableGestureInternally = false;
        disableGestureDynamically = false;
        dialMode = false;
        suppressInvalidaton = false;
        angleSnap = 0;
        angleSnapTemp = 0;
        angleSnapAngleDifference = 0;
        angleSnapToleranceTemp = 0;
        angleSnapTolerance = 0;
        paddingIsExpanded = false;

        snapToCenter = false;
        angle = 0;
        angleDifference = 0;
        cumulativeAngle = 0;
        secondPoint = new Point();
        gestureCenterPoint = new Point();
        rotationCenterPoint = new Point();

        firstPointerId = 0;
        secondPointerId = 0;
        firstPointerIdx = 0;
        secondPointerIdx = 0;

        matrixVals = new float[9];
        origonalWidth = 0;
        origonalHeight = 0;
        paddingL = imageView.getPaddingLeft();
        paddingT = imageView.getPaddingTop();
        paddingR = imageView.getPaddingRight();
        paddingB = imageView.getPaddingBottom();

        firstTouchX = 0;
        firstTouchY = 0;
        secondTouchX = 0;
        secondTouchY = 0;

        scaledSize = new int[2];

        measureSpec = View.MeasureSpec.getSize(View.MeasureSpec.UNSPECIFIED);
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        imageView.measure(measureSpec, measureSpec);

        iHacerGestoDown = this;
        iHacerGestoMove = this;
        iHacerGestoUp = this;

        hacerGestoDown = new HacerGestoDown(imageView, HGFlags.MOVE) {};
        hacerGestoMove = new HacerGestoMove(imageView, HGFlags.MOVE) {};
        hacerGestoUp = new HacerGestoUp(imageView, HGFlags.MOVE) {};

        matrix = imageView.getImageMatrix();
        matrix.getValues(matrixVals);
        hgResult = new HGResult(matrixVals[Matrix.MSCALE_X], matrixVals[Matrix.MSCALE_Y], 0, 0, 0, 0, 0);
        hgResult.setWhatGesture(HGFlags.ROTATE);
        origonalWidth = imageView.getMeasuredWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight();
        origonalHeight = imageView.getMeasuredHeight() - imageView.getPaddingTop() - imageView.getPaddingBottom();;
        scaledSize[0] = origonalWidth;
        scaledSize[0] = origonalHeight;
        this.imageView = imageView;

    }//End public HGPinch(ImageView imageView)

    //Top of block standard event controllers down/move/up
    public void setFirstTouch(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return;

        }

        disableGestureInternally = false;
        scaledSize = getRotatedOrScaledSize(imageView);

        try {

            try {

                matrix = imageView.getImageMatrix();
                matrix.getValues(matrixVals);
                matrix.setValues(matrixVals);
                imageView.setImageMatrix(matrix);
                imageView.invalidate();

                int firstPointerId = 0;

                if(event.getPointerCount() == 1) {

                    firstPointerId = event.getPointerId(0);
                    firstTouchX = event.getX(firstPointerId);
                    firstTouchY = event.getY(firstPointerId);
                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);
                    hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                    hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                    if(dialMode == true) {

                        setDialTouch();

                    }

                }//End if(event.getPointerCount() == 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setFirstTouch(MotionEvent event)


    public void setSecondTouch(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return;

        }

        if(dialMode == true) {

            return;

        }

        int secondPointerId = 0;

        try {

            try {

                if(event.getPointerCount() == 2) {

                    secondPointerId = event.getPointerId(1);
                    secondTouchX = event.getX(secondPointerId);
                    secondTouchY = event.getY(secondPointerId);
                    hgResult.setSecondTouchX(secondTouchX);
                    hgResult.setSecondTouchY(secondTouchY);
                    matrix.getValues(matrixVals);

                    rotationCenterPoint.x = (origonalWidth / 2);
                    rotationCenterPoint.y = (origonalHeight / 2);

                    if(snapToCenter == false) {

                        gestureCenterPoint.x = (int) (imageView.getPaddingLeft() + matrixVals[Matrix.MTRANS_X]);
                        gestureCenterPoint.x += (origonalWidth / 2) * (matrixVals[Matrix.MSKEW_X]);
                        gestureCenterPoint.x += (origonalWidth / 2) * (matrixVals[Matrix.MSCALE_X]);

                        gestureCenterPoint.y = (int) (imageView.getPaddingTop() + matrixVals[Matrix.MTRANS_Y]);
                        gestureCenterPoint.y += (origonalHeight / 2) * (matrixVals[Matrix.MSKEW_Y]);
                        gestureCenterPoint.y += (origonalHeight / 2) * (matrixVals[Matrix.MSCALE_Y]);

                    }
                    else {

                        gestureCenterPoint.x = imageView.getMeasuredWidth() / 2;
                        gestureCenterPoint.y = imageView.getMeasuredHeight() / 2;

                        if(snapToCenter == true) {

                            if(angleSnapTemp != 0) {

                                matrix.setRotate((angleSnapAngleDifference * angleSnapTemp) % 360, rotationCenterPoint.x, rotationCenterPoint.y);
                                imageView.setImageMatrix(matrix);
                                imageView.invalidate();
                                matrix.getValues(matrixVals);

                                angleSnapAngleDifference %= (360 / (angleSnapTemp * angleSnapToleranceTemp));

                            }
                            else {

                                matrix.setRotate(cumulativeAngle, rotationCenterPoint.x, rotationCenterPoint.y);
                                matrix.getValues(matrixVals);

                            }

                        }//End if(snapToCenter == true)

                    }//End if(snapToCenter == false)

                    secondPoint.set((int) secondTouchX, (int) secondTouchY);
                    angleDifference = (float) getAngleFromPoint(gestureCenterPoint, secondPoint) % 360;

                    hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                    hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);
                    hgResult.setScaleX(matrixVals[Matrix.MSCALE_X]);
                    hgResult.setScaleY(matrixVals[Matrix.MSCALE_Y]);

                    if(angleSnapTemp == 0) {//Default behaviour

                        hgResult.setAngle((angle + cumulativeAngle) % 360);

                    }
                    else {//Section executed when angle snap is enabled

                        if(angleSnapTemp != 0) {//Section executed when angle snap is enabled

                            hgResult.setAngle((angleSnapAngleDifference * angleSnapTemp) % 360);

                        }

                    }//End if(angleSnapTemp != 0)

                }//End if(event.getPointerCount() == 2)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setSecondTouch(MotionEvent event)


    public HGResult getDown(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        return hgResult;

    }//End public HGResult getDown(MotionEvent event)


    public HGResult getMotion(MotionEvent event) {

        //Dev Note: this if block is an unclean solution. Problem was when imageView is contained in it's own layout it wasn't working on older devices. Needs fixing (not priority.)
        if(imageView.isFocused() == false) {

            (imageView.getParent()).requestLayout();

        }

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(disableGestureInternally == true) {

            return hgResult;

        }

        //Top of block to call extendible behaviour methods that may be added to this class
        if(angleSnapTemp != 0) {

            angleSnapMotion(event);
            return hgResult;

        }
        //Bottom of block to call extendible behaviour methods that may be added to this class

        //Top of block executed code for default behaviour
        try {

            try {

                if(event.getPointerCount() > 1 || dialMode == true) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);
                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);

                    if(dialMode == false) {

                        secondPointerId = event.getPointerId(1);
                        secondPointerIdx = event.findPointerIndex(secondPointerId);
                        secondTouchX = event.getX(secondPointerIdx);
                        secondTouchY = event.getY(secondPointerIdx);
                        hgResult.setSecondTouchX(secondTouchX);
                        hgResult.setSecondTouchY(secondTouchY);
                        secondPoint.set((int) secondTouchX, (int) secondTouchY);

                    }
                    else {

                        firstTouchX = event.getX();
                        firstTouchY = event.getY();
                        hgResult.setFirstTouchX(firstTouchX);
                        hgResult.setFirstTouchY(firstTouchY);
                        secondPoint.set((int) firstTouchX, (int) firstTouchY);

                    }//End if(dialMode == false)

                    angle = (((float) getAngleFromPoint(gestureCenterPoint, secondPoint) - angleDifference) + 360) % 360;

                    if(suppressInvalidaton == false) {

                        matrix.setValues(matrixVals);
                        matrix.preRotate(angle, rotationCenterPoint.x, rotationCenterPoint.y);
                        imageView.setImageMatrix(matrix);
                        imageView.invalidate();

                    }

                    hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                    hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                    hgResult.setAngle((angle + cumulativeAngle) % 360);

                    return hgResult;

                }//End if(event.getPointerCount() > 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }
        //Bottom of block executed code for default behaviour

        return hgResult;

    }//End public HGResult getMotion(MotionEvent event)


    public HGResult getUp(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(disableGestureInternally == true) {

            firstPointerId = event.getPointerId(0);
            firstPointerIdx = event.findPointerIndex(firstPointerId);
            firstTouchX = event.getX(firstPointerIdx);
            firstTouchY = event.getY(firstPointerIdx);
            hgResult.setFirstTouchX(firstTouchX);
            hgResult.setFirstTouchY(firstTouchY);

            return hgResult;

        }
        else {

            if(event.getPointerCount() > 1 || dialMode == true) {

                disableGestureInternally = true;

                firstPointerId = event.getPointerId(0);
                firstPointerIdx = event.findPointerIndex(firstPointerId);
                firstTouchX = event.getX(firstPointerIdx);
                firstTouchY = event.getY(firstPointerIdx);
                hgResult.setFirstTouchX(firstTouchX);
                hgResult.setFirstTouchY(firstTouchY);

                if(dialMode == false) {

                    secondPointerId = event.getPointerId(1);
                    secondPointerIdx = event.findPointerIndex(secondPointerId);
                    secondTouchX = event.getX(secondPointerIdx);
                    secondTouchY = event.getY(secondPointerIdx);
                    hgResult.setSecondTouchX(secondTouchX);
                    hgResult.setSecondTouchY(secondTouchY);

                }

                if(angleSnapTemp == 0) {//Default behaviour

                    angle = (((float) getAngleFromPoint(gestureCenterPoint, secondPoint) - angleDifference) + 360) % 360;
                    matrix.setValues(matrixVals);
                    matrix.preRotate(angle, rotationCenterPoint.x, rotationCenterPoint.y);
                    imageView.setImageMatrix(matrix);
                    imageView.invalidate();
                    hgResult.setAngle((angle + cumulativeAngle) % 360);
                    cumulativeAngle = (cumulativeAngle + angle) % 360;

                }
                else {//Section executed when angle snap is enabled

                    if(angleSnapTemp != 0) {//Section executed when angle snap is enabled

                        if(angleSnap - Math.round((angleSnapTolerance / angleSnapTemp) / angleSnapToleranceTemp)  == 0) {

                            matrix.setValues(matrixVals);
                            matrix.preRotate((angleSnap * angleSnapTemp) % 360, rotationCenterPoint.x, rotationCenterPoint.y);
                            imageView.setImageMatrix(matrix);
                            imageView.invalidate();

                            angleSnapAngleDifference += angleSnap;
                            angleSnapAngleDifference %= (360 / (angleSnapTemp * angleSnapToleranceTemp));
                            hgResult.setAngle((angleSnapAngleDifference * angleSnapTemp) % 360);

                        }//End if(angleSnap - Math.round((angleSnapTolerance / angleSnapTemp) / angleSnapToleranceTemp)  == 0)

                    }//End if(angleSnapTemp != 0)

                }//End if(angleSnapTemp != 0)

            }
            else if(event.getPointerCount() == 1) {//Fault tolerance contition should not met

                disableGestureInternally = false;

            }//End if(event.getPointerCount() > 1)

            matrix.getValues(matrixVals);
            hgResult.setScaleX(matrixVals[Matrix.MSCALE_X]);
            hgResult.setScaleY(matrixVals[Matrix.MSCALE_Y]);
            hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
            hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);
            scaledSize = getRotatedOrScaledSize(imageView);

            return hgResult;

        }//End if(disableGestureInternally == true)

    }//End public HGResult getUp(MotionEvent event)
    //Bottom of block standard event controllers down/move/up


    //Method retuns an angle based between two points, the first parameter is for the center point.
    public double getAngleFromPoint(Point firstPoint, Point secondPoint) {

        if((secondPoint.x > firstPoint.x)) {//above 0 to 180 degrees

            return (Math.atan2((secondPoint.x - firstPoint.x), (firstPoint.y - secondPoint.y)) * 180 / Math.PI);

        }
        else if((secondPoint.x < firstPoint.x)) {//above 180 degrees to 360/0

            return 360 - (Math.atan2((firstPoint.x - secondPoint.x), (firstPoint.y - secondPoint.y)) * 180 / Math.PI);

        }//End if((secondPoint.x > firstPoint.x) && (secondPoint.y <= firstPoint.y))

        return Math.atan2(0, 0);

    }//End public float getAngleFromPoint(Point firstPoint, Point secondPoint)


    //Top of Block behavioural methods
    public void setDialTouch() {

        try {

            try {

                hgResult.setSecondTouchX(secondTouchX);
                hgResult.setSecondTouchY(secondTouchY);
                matrix.getValues(matrixVals);

                if(snapToCenter == false) {

                    gestureCenterPoint.x = (int) (imageView.getPaddingLeft() + matrixVals[Matrix.MTRANS_X]);
                    gestureCenterPoint.x += (origonalWidth / 2) * (matrixVals[Matrix.MSKEW_X]);
                    gestureCenterPoint.x += (origonalWidth / 2) * (matrixVals[Matrix.MSCALE_X]);

                    gestureCenterPoint.y = (int) (imageView.getPaddingTop() + matrixVals[Matrix.MTRANS_Y]);
                    gestureCenterPoint.y += (origonalHeight / 2) * (matrixVals[Matrix.MSKEW_Y]);
                    gestureCenterPoint.y += (origonalHeight / 2) * (matrixVals[Matrix.MSCALE_Y]);

                }
                else {

                    matrixVals[Matrix.MTRANS_X] = 0;
                    matrixVals[Matrix.MSKEW_X] = 0;
                    matrixVals[Matrix.MSCALE_X] = 1;

                    matrixVals[Matrix.MTRANS_Y] = 0;
                    matrixVals[Matrix.MSKEW_Y] = 0;
                    matrixVals[Matrix.MSCALE_Y] = 1;
                    angle = 0;
                    cumulativeAngle = 0;
                    angleDifference = 0;

                    gestureCenterPoint.x = imageView.getMeasuredWidth() / 2;
                    gestureCenterPoint.y = imageView.getMeasuredHeight() / 2;

                }//End if(snapToCenter == false)

                rotationCenterPoint.x = (origonalWidth / 2);
                rotationCenterPoint.y = (origonalHeight / 2);
                secondPoint.set((int) firstTouchX, (int) firstTouchY);
                angleDifference = (float) getAngleFromPoint(gestureCenterPoint, secondPoint) % 360;

                hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                if(angleSnapTemp == 0) {//Default behaviour

                    hgResult.setAngle((angle + cumulativeAngle) % 360);

                }
                else {//Section executed when angle snap is enabled

                    if(angleSnapTemp != 0) {//Section executed when angle snap is enabled

                        hgResult.setAngle((angleSnapAngleDifference * angleSnapTemp) % 360);

                    }

                }//End if(angleSnapTemp != 0)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setDialTouch()


    public void angleSnapMotion(MotionEvent event) {

        try {

            try {

                if(event.getPointerCount() > 1 || dialMode == true) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);
                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);

                    if(dialMode == false) {

                        secondPointerId = event.getPointerId(1);
                        secondPointerIdx = event.findPointerIndex(secondPointerId);
                        secondTouchX = event.getX(secondPointerIdx);
                        secondTouchY = event.getY(secondPointerIdx);
                        hgResult.setSecondTouchX(secondTouchX);
                        hgResult.setSecondTouchY(secondTouchY);
                        secondPoint.set((int) secondTouchX, (int) secondTouchY);

                    }
                    else {

                        firstTouchX = event.getX();
                        firstTouchY = event.getY();
                        hgResult.setFirstTouchX(firstTouchX);
                        hgResult.setFirstTouchY(firstTouchY);
                        secondPoint.set((int) firstTouchX, (int) firstTouchY);

                    }//End if(dialMode == false)

                    angle = (((float) getAngleFromPoint(gestureCenterPoint, secondPoint) - angleDifference) + 360);

                    hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                    hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                    angleSnapTolerance = (((Math.abs(360 - -(angle)) + (Math.abs((angle) + 360))) / 2) % 360);

                    if((int) angleSnapTolerance != 0) {

                        if(angleSnapTolerance > ((angleSnap * angleSnapToleranceTemp))) {

                            angleSnap = Math.round((angleSnapTolerance / angleSnapTemp) / angleSnapToleranceTemp);
                            angleSnap %= (360 / (angleSnapTemp * angleSnapToleranceTemp));
                            hgResult.setAngle((((angleSnap + angleSnapAngleDifference) % (360 / (angleSnapTemp * angleSnapToleranceTemp))) * angleSnapTemp) % 360);

                            if(suppressInvalidaton == false) {

                                matrix.setValues(matrixVals);
                                matrix.preRotate((angleSnap * angleSnapTemp) % 360, rotationCenterPoint.x, rotationCenterPoint.y);
                                imageView.setImageMatrix(matrix);
                                imageView.invalidate();

                            }

                        }//End if(angleSnapTolerance > ((angleSnap * angleSnapToleranceTemp)))

                    }//End if((int) angleSnapTolerance != 0)

                    return;

                }//End if(event.getPointerCount() > 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

        return;

    }//End public void angleSnapMotion(MotionEvent event)


    public int[] getRotatedOrScaledSize() {

        return scaledSize;

    }


    public void setSnapToCenter(boolean snapToCenter) {

        this.snapToCenter = snapToCenter;

    }


    public boolean getSnapToCenter() {

        return snapToCenter;

    }


    public void setDialMode(boolean dialMode) {

        this.dialMode = dialMode;

    }


    public boolean getDialMode() {

        return dialMode;

    }


    public void setAngleSnap(float angleSnap, float angleSnapTolerance) {

        this.angleSnap = 0;
        angleSnapTemp = angleSnap;
        this.angleSnapTolerance = angleSnapTolerance;
        angleSnapToleranceTemp = angleSnapTolerance;

    }


    public float[] getAngleSnap() {

        float[] returnVal = new float[2];
        returnVal[0] = angleSnap; returnVal[1] = angleSnapTolerance;
        return returnVal;

    }
    //Bottom of Block behavioural methods


    //Top of Block convenience methods
    public int[] getRotatedOrScaledSize(ImageView imageView) {

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        scaledSize[0] = bitmap.getWidth();
        scaledSize[1] = bitmap.getHeight();

        return scaledSize;

    }//End public int[] getRotatedOrScaledSize(ImageView imageView)


    public Bitmap getRotatedBitmap(ImageView imageView) {

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return bitmap;

    }


    public void manualRotate(float angle, Point rotationCenterPoint) {

        cumulativeAngle += angle;

        if(cumulativeAngle < 0) {cumulativeAngle = 360 - Math.abs(cumulativeAngle);}

        matrix = imageView.getImageMatrix();
        matrix.setValues(matrixVals);
        matrix.preRotate(angle, rotationCenterPoint.x, rotationCenterPoint.y);
        matrix.getValues(matrixVals);
        imageView.setImageMatrix(matrix);
        imageView.invalidate();

    }//End public void manualRotate(float angle, Point rotationCenterPoint)
    //Bottom of Block convenience methods


    //Top of Block Common Functions
    public Bitmap getBitmapFromImageView(ImageView imageView) {

        return ((BitmapDrawable) imageView.getDrawable()).getBitmap();

    }


    //Open end for extensibility
    public void setMatrixVals(float[] matrixVals) {

        //

    }

    public float[] getMatrixVals() {

        matrix.getValues(matrixVals);
        return matrixVals;

    }


    public void suppressInvalidaton(boolean suppressInvalidaton) {

        this.suppressInvalidaton = suppressInvalidaton;

    }


    public boolean getSuppressInvalidaton() {

        return suppressInvalidaton;

    }


    public void onDown(HGResult hgResult) {

        hacerGestoDown = null;

    }


    public void onMotion(HGResult hgResult) {

        hacerGestoMove = null;

    }


    public void onRelease(HGResult hgResult) {

        hacerGestoUp = null;

    }

    public HGResult getHgResult() {

        return this.hgResult;

    }


    public void setDisableGesture(boolean disableGestureDynamically) {

        this.disableGestureDynamically = disableGestureDynamically;

    }


    public boolean isGesturedisabled() {

        return disableGestureDynamically;

    }


    public void expandPaddingToFitContainer(final boolean expandPadding) {

        paddingIsExpanded = expandPadding;

        origonalWidth = imageView.getMeasuredWidth();
        origonalHeight = imageView.getMeasuredHeight();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {

                                if (expandPadding == true) {

                                    paddingL = imageView.getLeft();
                                    paddingT = imageView.getTop();
                                    paddingR = ((ViewGroup) imageView.getParent()).getMeasuredWidth() - imageView.getRight();
                                    paddingB = ((ViewGroup) imageView.getParent()).getMeasuredHeight() - imageView.getBottom();
                                    imageView.setPadding(paddingL, paddingT, paddingR, paddingB);

                                }
                                else {

                                    paddingL = imageView.getPaddingLeft();
                                    paddingT = imageView.getPaddingTop();
                                    paddingR = imageView.getPaddingRight();
                                    paddingB = imageView.getPaddingBottom();
                                    imageView.setPadding(0, 0, 0, 0);

                                }//End if(expandPadding == true)

                                (imageView.getParent()).requestLayout();

                            }

                        }, 10);

            }

        });

    }//End public void expandPaddingToFitContainer(final boolean expandPadding)
    //Bottom of Block Common Functions


    @Override
    public boolean onTouch(View view, MotionEvent event) {

        final int action = event.getAction() & MotionEvent.ACTION_MASK;

        switch(action) {

            case MotionEvent.ACTION_DOWN: {

                setFirstTouch(event);

                if(hacerGestoDown != null) {

                    iHacerGestoDown.onDown(getDown(event));

                }
                else {

                    getDown(event);

                }

                break;

            }
            case MotionEvent.ACTION_POINTER_DOWN: {

                setSecondTouch(event);

                if(hacerGestoDown != null) {

                    iHacerGestoDown.onDown(getDown(event));

                }
                else {

                    getDown(event);

                }//End if(hacerGestoDown != null)

                break;

            }
            case MotionEvent.ACTION_MOVE: {

                if(hacerGestoMove != null) {

                    iHacerGestoMove.onMotion(getMotion(event));

                }

                break;

            }
            case MotionEvent.ACTION_POINTER_UP: {

                if(hacerGestoUp != null) {

                    iHacerGestoUp.onRelease(getUp(event));

                }
                else {

                    getUp(event);

                }//End if(hacerGestoUp != null)

                break;

            }
            case MotionEvent.ACTION_UP: {

                if(hacerGestoUp != null) {

                    iHacerGestoUp.onRelease(getUp(event));

                }
                else {

                    getUp(event);

                }

                break;

            }
            default:

                break;

        }//End switch(action)

        return true;

    }//End public boolean onTouch(View view, MotionEvent event)

}