package com.WarwickWestonWright.HacerGesto;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public abstract class HacerGesto implements View.OnTouchListener, HacerGestoDown.IHacerGestoDown, HacerGestoMove.IHacerGestoMove, HacerGestoUp.IHacerGestoUp {

    private ImageView imageView;

    private HacerGestoDown.IHacerGestoDown iHacerGestoDown = null;
    private HacerGestoMove.IHacerGestoMove iHacerGestoMove = null;
    private HacerGestoUp.IHacerGestoUp iHacerGestoUp = null;

    private static HacerGestoDown hacerGestoDown = null;
    private static HacerGestoMove hacerGestoMove = null;
    private static HacerGestoUp hacerGestoUp = null;

    private static boolean isMoveActive = false;
    private static boolean isFlingActive = false;
    private static boolean isScaleActive = false;
    private static boolean isRotateActive = false;

    private HGMove hgMove = null;
    private HGFling hgFling = null;
    private HGScale hgScale = null;
    private HGRotate hgRotate = null;

    private int hgFlag;
    private static HGResult hgResult;

    private static Matrix matrix = null;
    private static float[] matrixVals = null;

    private static int measureSpec = View.MeasureSpec.getSize(View.MeasureSpec.UNSPECIFIED);

    public HacerGesto(ImageView imageView, int hgFlag) {

        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        imageView.measure(measureSpec, measureSpec);

        matrixVals = new float[9];
        matrix = imageView.getImageMatrix();
        matrix.getValues(matrixVals);

        this.imageView = imageView;
        this.hgFlag = hgFlag;

        iHacerGestoDown = this;
        iHacerGestoMove = this;
        iHacerGestoUp = this;

        hacerGestoDown = new HacerGestoDown(imageView, hgFlag) {};
        hacerGestoMove = new HacerGestoMove(imageView, hgFlag) {};
        hacerGestoUp = new HacerGestoUp(imageView, hgFlag) {};

        isMoveActive = false;
        isFlingActive = false;
        isScaleActive = false;
        isRotateActive = false;

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            hgRotate = new HGRotate(imageView) {};
            isRotateActive = true;

        }
        else {

            isRotateActive = false;

        }

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            hgScale = new HGScale(imageView) {};
            isScaleActive = true;

        }
        else {

            isScaleActive = false;

        }

        if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE) {

            hgMove = new HGMove(imageView) {};
            isMoveActive = true;

        }
        else {

            isMoveActive = false;

        }

        if((hgFlag & HGFlags.FLING) == HGFlags.FLING) {

            hgFling = new HGFling(imageView) {};
            isFlingActive = true;

        }
        else {

            isFlingActive = false;

        }

    }//End public HacerGesto(View view, int hgFlag)


    //Convenience method
    public void expandPaddingToFitContainer(final boolean expandPadding) {

        if(hgRotate != null) {

            hgRotate.expandPaddingToFitContainer(expandPadding);

        }//End if(hgMove != null)

        if(hgScale != null) {

            hgScale.expandPaddingToFitContainer(expandPadding);

        }

        if(hgMove != null) {

            hgMove.expandPaddingToFitContainer(expandPadding);

        }

        if(hgFling != null) {

            hgFling.expandPaddingToFitContainer(expandPadding);

        }

    }//End public void expandPaddingToFitContainer(final boolean expandPadding)


    public void setCumaulativeMove(boolean cumaulativeMove) {

        if(hgMove != null) {

            hgMove.setCumaulativeMove(cumaulativeMove);

        }

    }//End public void setCumaulativeMove(boolean cumaulativeMove)


    public boolean getCumaulativeMove() {

        if(hgMove != null) {

            return hgMove.getCumaulativeMove();

        }

        return false;

    }//End public boolean getCumaulativeMove()


    public void setupFlingValues(int flingDistanceThreashold, int flingTimeThreashold, int flingAnimationTime) {

        if(hgFling != null) {

            hgFling.setupFlingValues(flingDistanceThreashold, flingTimeThreashold, flingAnimationTime);

        }

    }//End public void setupFlingValues(int flingDistanceThreashold, int flingTimeThreashold, int flingAnimationTime)


    public void setFlingDistance(int dynamicFlingDistance) {

        if(hgFling != null) {

            hgFling.setFlingDistance(dynamicFlingDistance);

        }

    }//End public void setFlingDistance(int dynamicFlingDistance)


    public int getFlingDistance() {

        if(hgFling != null) {

            return hgFling.getFlingDistance();

        }
        else {

            return 0;

        }

    }//End public int getFlingDistance()


    public void setFlingBounceBack(boolean bounceBack) {

        if(hgFling != null) {

            hgFling.setFlingBounceBack(bounceBack);

        }

    }//End public void setFlingBounceBack(boolean bounceBack)


    public boolean getBounceBack() {

        if(hgFling != null) {

            return hgFling.getBounceBack();

        }
        else {

            return false;

        }

    }//End public boolean getBounceBack()


    public void triggerFling(int dynamicFlingDistance, int width, int height) {

        if(hgFling != null) {

            hgFling.triggerFling(dynamicFlingDistance, width, height);

        }

    }//End public void triggerFling(int dynamicFlingDistance, int width, int height)


    public void disableGesture(boolean disableGestureDynamically, int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                if(disableGestureDynamically == false) {

                    hgRotate = new HGRotate(imageView) {};//Reset gesture if already enabled
                    hgRotate.setDisableGesture(disableGestureDynamically);

                }
                else if(disableGestureDynamically == true) {

                    hgRotate.setDisableGesture(disableGestureDynamically);
                    hgRotate = null;

                }//End if(disableGestureDynamically == false)

            }
            else if(hgRotate == null) {

                if(disableGestureDynamically == false) {

                    hgRotate = new HGRotate(imageView) {};
                    hgRotate.setDisableGesture(disableGestureDynamically);

                }

            }//End if(hgRotate != null)

        }//End if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE)

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                if(disableGestureDynamically == false) {

                    hgScale = new HGScale(imageView) {};//Reset gesture if already enabled
                    hgScale.setDisableGesture(disableGestureDynamically);

                }
                else if(disableGestureDynamically == true) {

                    hgScale.setDisableGesture(disableGestureDynamically);
                    hgScale = null;

                }//End if(disableGestureDynamically == false)

            }
            else if(hgScale == null) {

                if(disableGestureDynamically == false) {

                    hgScale = new HGScale(imageView) {};
                    hgScale.setDisableGesture(disableGestureDynamically);

                }

            }//End if(hgRotate != null)

        }//End if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE)

        if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE) {

            if(hgMove != null) {

                if(disableGestureDynamically == false) {

                    hgMove = new HGMove(imageView) {};//Reset gesture if already enabled
                    hgMove.setDisableGesture(disableGestureDynamically);

                }
                else if(disableGestureDynamically == true) {

                    hgMove.setDisableGesture(disableGestureDynamically);
                    hgMove = null;

                }//End if(disableGestureDynamically == false)

            }
            else if(hgMove == null) {

                if(disableGestureDynamically == false) {

                    hgMove = new HGMove(imageView) {};
                    hgMove.setDisableGesture(disableGestureDynamically);

                }

            }//End if(hgMove != null)

        }//End if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE)

        if((hgFlag & HGFlags.FLING) == HGFlags.FLING) {

            if(hgFling != null) {

                if(disableGestureDynamically == false) {

                    hgFling = new HGFling(imageView) {};//Reset gesture if already enabled
                    hgFling.setDisableGesture(disableGestureDynamically);

                }
                else if(disableGestureDynamically == true) {

                    hgFling.setDisableGesture(disableGestureDynamically);
                    hgFling = null;

                }//End if(disableGestureDynamically == false)

            }
            else if(hgFling == null) {

                if(disableGestureDynamically == false) {

                    hgFling = new HGFling(imageView) {};
                    hgFling.setDisableGesture(disableGestureDynamically);

                }

            }//End if(hgFling != null)

        }//End if((hgFlag & HGFlags.FLING) == HGFlags.FLING)

    }//End public void disableGesture(boolean disableGestureDynamically, int hgFlag)


    public void suppressInvalidaton(boolean suppressInvalidaton, int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                hgRotate.suppressInvalidaton(suppressInvalidaton);

            }

        }//End if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE)

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                hgScale.suppressInvalidaton(suppressInvalidaton);

            }

        }//End if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE)

        if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE) {

            if(hgMove != null) {

                hgMove.suppressInvalidaton(suppressInvalidaton);

            }

        }//End if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE)

        if((hgFlag & HGFlags.FLING) == HGFlags.FLING) {

            //Not yet implemented as fling does not use onMotion.

        }//End if((hgFlag & HGFlags.FLING) == HGFlags.FLING)

    }//End public void suppressInvalidaton(boolean suppressInvalidaton, int hgFlag)


    public boolean getSuppressInvalidaton(int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                return hgRotate.getSuppressInvalidaton();

            }

        }//End if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE)

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                return hgScale.getSuppressInvalidaton();

            }

        }//End if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE)

        if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE) {

            if(hgMove != null) {

                return hgMove.getSuppressInvalidaton();

            }

        }//End if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE)

        if((hgFlag & HGFlags.FLING) == HGFlags.FLING) {

            //Not yet implemented as fling does not use onMotion.

        }//End if((hgFlag & HGFlags.FLING) == HGFlags.FLING)

        return false;

    }//End public boolean getSuppressInvalidaton(int hgFlag)


    public boolean isGesturedisabled(int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                return hgRotate.isGesturedisabled();

            }

        }

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                return hgScale.isGesturedisabled();

            }

        }

        if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE) {

            if(hgMove != null) {

                return hgMove.isGesturedisabled();

            }

        }

        if((hgFlag & HGFlags.FLING) == HGFlags.FLING) {

            if(hgFling != null) {

                return hgFling.isGesturedisabled();

            }

        }

        return false;

    }//End public boolean isGesturedisabled(int hgFlag)


    public void setSnapToCenter(boolean snapToCenter, int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                hgRotate.setSnapToCenter(snapToCenter);

            }

        }

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                hgScale.setSnapToCenter(snapToCenter);

            }

        }

    }//End public void setSnapToCenter(boolean snapToCenter, int hgFlag)


    public boolean getSnapToCenter(int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                hgRotate.getSnapToCenter();

            }

        }

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                return hgScale.getSnapToCenter();

            }

        }

        return false;

    }//End public boolean getSnapToCenter(int hgFlag)


    public void setDialMode(boolean dialMode) {

        if(hgRotate != null) {

            hgRotate.setDialMode(dialMode);

        }

    }//End public void setDialMode(boolean dialMode)


    public boolean getDialMode() {

        if(hgRotate != null) {

            return hgRotate.getDialMode();

        }
        else {

            return false;

        }

    }//End public boolean getDialMode()


    public void setAngleSnap(float angleSnap, float angleSnapTolerance) {

        if(hgRotate != null) {

            hgRotate.setAngleSnap(angleSnap, angleSnapTolerance);

        }

    }//End public void setAngleSnap(float angleSnap, float angleSnapTolerance)


    public float[] getAngleSnap() {

        if(hgRotate != null) {

            return hgRotate.getAngleSnap();

        }

        return new float[2];

    }//End public float[] getAngleSnap()


    public void setMoveSnap(Point[] snapPositions, float snapTolerances[]) {

        if(hgMove != null) {

            hgMove.setMoveSnap(snapPositions, snapTolerances);

        }

    }


    public Point[] getMoveSnapPositions() {

        if(hgMove != null) {

            return hgMove.getMoveSnapPositions();

        }

        return null;

    }


    public float[] getMoveSnapTolerances() {

        if(hgMove != null) {

            return hgMove.getMoveSnapTolerance();

        }

        return null;

    }


    public float[] getScaledSize() {

        if(hgScale != null) {

            hgScale.getScaledSize();

        }

        return null;

    }


    public void manualRotate(float angle, Point rotationCenterPoint) {

        if(hgRotate != null) {

            hgRotate.manualRotate(angle, rotationCenterPoint);

        }

    }


    public void manualScale(float scaleX, float scaleY) {

        if(hgScale != null) {

            hgScale.manualScale(scaleX, scaleY);

        }

    }//End public void manualScale(float scaleX, float scaleY)


    public void manualMove(float xPos, float yPos) {

        if(hgMove != null) {

            hgMove.manualMove(xPos, yPos);

        }

    }


    public Bitmap getBitmapFromImageView(ImageView imageView) {

        return ((BitmapDrawable) imageView.getDrawable()).getBitmap();

    }


    public void setMinMaxSnap(float minimumSnap, float maximumSnap, float snapTolerance) {

        if(hgScale != null) {

            hgScale.setMinMaxSnap(minimumSnap, maximumSnap, snapTolerance);

        }

    }


    public float[] getMinMaxSnap(float minimumSnap, float MaximumSnap, float snapTolerance) {

        if(hgScale != null) {

            hgScale.getMinMaxSnap();

        }

        return null;

    }


    public void setMatrixVals(float[] matrixVals, int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                hgRotate.setMatrixVals(matrixVals);

            }

        }

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                hgScale.setMatrixVals(matrixVals);

            }

        }

        if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE) {

            if(hgMove != null) {

                hgMove.setMatrixVals(matrixVals);

            }

        }

        if((hgFlag & HGFlags.FLING) == HGFlags.FLING) {

            if(hgFling != null) {

                hgFling.setMatrixVals(matrixVals);

            }

        }

    }//End public void setMatrixVals(float[] matrixVals)


    public Bitmap getProportionalBitmap(Bitmap bitmap, int newDimensionXorY, String XorY) {

        if(hgScale != null) {

            return hgScale.getProportionalBitmap(bitmap, newDimensionXorY, XorY);

        }

        return null;

    }


    public Bitmap getRotatedBitmap(ImageView imageView) {

        if(hgRotate != null) {

            return hgRotate.getRotatedBitmap(imageView);

        }

        return null;

    }


    public int[] getRotatedOrScaledSize(ImageView imageView, int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                return hgRotate.getRotatedOrScaledSize(imageView);

            }

        }
        else if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            return hgScale.getRotatedOrScaledSize(imageView);

        }

        return null;

    }//End public int[] getRotatedOrScaledSize(ImageView imageView)


    public Bitmap getBitmapFromImageView(ImageView imageView, int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                return hgRotate.getBitmapFromImageView(imageView);

            }

        }

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                return hgScale.getBitmapFromImageView(imageView);

            }

        }

        if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE) {

            if(hgMove != null) {

                return hgMove.getBitmapFromImageView(imageView);

            }

        }

        if((hgFlag & HGFlags.FLING) == HGFlags.FLING) {

            if(hgFling != null) {

                return hgFling.getBitmapFromImageView(imageView);

            }

        }

        return null;

    }//End public Bitmap getBitmapFromImageView(ImageView imageView, int hgFlag)


    public float[] getMatrixVals(int hgFlag) {

        if((hgFlag & HGFlags.ROTATE) == HGFlags.ROTATE) {

            if(hgRotate != null) {

                return hgRotate.getMatrixVals();

            }

        }

        if((hgFlag & HGFlags.SCALE) == HGFlags.SCALE) {

            if(hgScale != null) {

                return hgScale.getMatrixVals();

            }

        }

        if((hgFlag & HGFlags.MOVE) == HGFlags.MOVE) {

            if(hgMove != null) {

                return hgMove.getMatrixVals();

            }

        }

        if((hgFlag & HGFlags.FLING) == HGFlags.FLING) {

            if(hgFling != null) {

                return hgFling.getMatrixVals();

            }

        }

        return null;

    }//End public float[] getMatrixVals()


    public void onDown(HGResult hgResult) {

        hacerGestoDown = null;

    }


    public void onMotion(HGResult hgResult) {

        hacerGestoMove = null;

    }


    public void onRelease(HGResult hgResult) {

        hacerGestoUp = null;

    }
    /* Developer note end */


    @Override
    public boolean onTouch(View view, MotionEvent event) {

        final int action = event.getAction() & MotionEvent.ACTION_MASK;

        switch(action) {

            case MotionEvent.ACTION_DOWN: {

                if(hacerGestoDown != null) {

                    //Top of block Set MatrixVals Section
                    if(hgRotate != null) {

                        if(hgScale != null) {

                            hgScale.setMatrixVals(hgRotate.getMatrixVals());

                        }//End if(isScaleActive == true)

                        if(hgMove != null) {

                            hgMove.setMatrixVals(hgRotate.getMatrixVals());

                        }//End if(isMoveActive == true)

                        if(hgFling != null) {

                            hgFling.setMatrixVals(hgRotate.getMatrixVals());

                        }//End if(isFlingActive == true)

                    }
                    else if(hgScale != null) {

                        if(hgMove != null) {

                            hgMove.setMatrixVals(hgScale.getMatrixVals());

                        }//End if(isMoveActive == true)

                        if(hgFling != null) {

                            hgFling.setMatrixVals(hgScale.getMatrixVals());

                        }//End if(isFlingActive == true)

                    }
                    else if(hgMove != null) {

                        if(hgFling != null) {

                            hgFling.setMatrixVals(hgMove.getMatrixVals());

                        }//End if(isFlingActive == true)

                    }
                    else if(hgFling != null) {

                        //Open end to add another gesture that only affects one factor

                    }//End if(isRotateActive == true)
                    //Bottom of block Set MatrixVals Section

                    //Top of block Gesture Section
                    if(hgFling != null) {

                        hgFling.setFirstTouch(event);
                        iHacerGestoDown.onDown(hgFling.getDown(event));

                    }

                    if(hgMove != null) {

                        hgMove.setFirstTouch(event);
                        iHacerGestoDown.onDown(hgMove.getDown(event));

                    }

                    if(hgScale != null) {

                        hgScale.setFirstTouch(event);
                        iHacerGestoDown.onDown(hgScale.getDown(event));

                    }

                    if(hgRotate != null) {

                        hgRotate.setFirstTouch(event);
                        iHacerGestoDown.onDown(hgRotate.getDown(event));

                    }
                    //Bottom of block Gesture Section

                }
                else if(hacerGestoDown == null) {

                    if(hgFling != null) {

                        hgFling.setFirstTouch(event);

                    }

                    if(hgMove != null) {

                        hgMove.setFirstTouch(event);

                    }

                    if(hgScale != null) {

                        hgScale.setFirstTouch(event);

                    }

                    if(hgRotate != null) {

                        hgRotate.setFirstTouch(event);

                    }

                }//End if(hacerGestoDown != null)

                break;

            }
            case MotionEvent.ACTION_POINTER_DOWN: {

                if(hacerGestoDown != null) {

                    if(hgRotate != null) {

                        hgRotate.setSecondTouch(event);
                        iHacerGestoDown.onDown(hgRotate.getDown(event));

                    }

                    if(hgScale != null) {

                        hgScale.setSecondTouch(event);
                        iHacerGestoDown.onDown(hgScale.getDown(event));

                    }

                    if(hgMove != null) {

                        hgMove.setSecondTouch(event);
                        iHacerGestoDown.onDown(hgMove.getDown(event));

                    }

                    if(hgFling != null) {

                        hgFling.setSecondTouch(event);
                        iHacerGestoDown.onDown(hgFling.getDown(event));

                    }

                }
                else if(hacerGestoDown == null) {

                    if(hgRotate != null) {

                        hgRotate.setSecondTouch(event);

                    }

                    if(hgScale != null) {

                        hgScale.setSecondTouch(event);

                    }

                    if(hgMove != null) {

                        hgMove.setSecondTouch(event);

                    }

                    if(hgFling != null) {

                        hgFling.setSecondTouch(event);

                    }

                }//End if(hacerGestoDown != null)

                break;

            }
            case MotionEvent.ACTION_MOVE: {

                if(hacerGestoMove != null) {

                    if(hgFling != null) {

                        iHacerGestoMove.onMotion(hgFling.getMotion(event));

                    }

                    if(hgMove != null) {

                        iHacerGestoMove.onMotion(hgMove.getMotion(event));

                    }

                    if(hgScale != null) {

                        iHacerGestoMove.onMotion(hgScale.getMotion(event));

                    }

                    if(hgRotate != null) {

                        iHacerGestoMove.onMotion(hgRotate.getMotion(event));

                    }

                }
                else {

                    if(hgFling != null) {

                        hgFling.getMotion(event);

                    }

                    if(hgMove != null) {

                        hgMove.getMotion(event);

                    }

                    if(hgScale != null) {

                        hgScale.getMotion(event);

                    }

                    if(hgRotate != null) {

                        hgRotate.getMotion(event);

                    }

                }//End if(hacerGestoMove != null)

                break;

            }
            case MotionEvent.ACTION_POINTER_UP: {

                if(hacerGestoUp != null) {

                    if(hgFling != null) {

                        iHacerGestoUp.onRelease(hgFling.getUp(event));

                    }

                    if(hgMove != null) {

                        iHacerGestoUp.onRelease(hgMove.getUp(event));

                    }

                    if(hgScale != null) {

                        iHacerGestoUp.onRelease(hgScale.getUp(event));

                    }

                    if(hgRotate != null) {

                        iHacerGestoUp.onRelease(hgRotate.getUp(event));

                    }

                }
                else if(hacerGestoUp == null) {

                    if(hgFling != null) {

                        hgFling.getUp(event);

                    }

                    if(hgMove != null) {

                        hgMove.getUp(event);

                    }

                    if(hgScale != null) {

                        hgScale.getUp(event);

                    }

                    if(hgRotate != null) {

                        hgRotate.getUp(event);

                    }

                }//End if(hacerGestoUp != null)

                break;

            }
            case MotionEvent.ACTION_UP: {

                if(hacerGestoUp != null) {

                    if(hgFling != null) {

                        iHacerGestoUp.onRelease(hgFling.getUp(event));

                    }

                    if(hgMove != null) {

                        iHacerGestoUp.onRelease(hgMove.getUp(event));

                    }

                    if(hgScale != null) {

                        iHacerGestoUp.onRelease(hgScale.getUp(event));

                    }

                    if(hgRotate != null) {

                        iHacerGestoUp.onRelease(hgRotate.getUp(event));

                    }

                }
                else if(hacerGestoUp == null) {

                    if(hgFling != null) {

                        hgFling.getUp(event);

                    }

                    if(hgMove != null) {

                        hgMove.getUp(event);

                    }

                    if(hgScale != null) {

                        hgScale.getUp(event);

                    }

                    if(hgRotate != null) {

                        hgRotate.getUp(event);

                    }

                }//End if(hacerGestoUp != null)

                break;

            }
            default:

                break;

        }//End switch(action)

        return true;

    }//End public boolean onTouch(View view, MotionEvent event)

}