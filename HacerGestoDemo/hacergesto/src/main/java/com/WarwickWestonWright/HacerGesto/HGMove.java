/*
Notes about this class: The encapsulation between this class and the other gesture classes is absolute with the exception of the setMatrixVals(float[] matrixVals) method.
When extending this class please refer the the *Extensibility notes:* in the overview section of the repo at: https://bitbucket.org/warwick/hacergestov1
There is a section in this class with a comment block '//Top of block to call extendible behaviour methods that may be added to this class'
It is in this section that I (the developer) recommend extensible methods should be called. There is a function here 'moveSnapMotion(event)'. Examining how this function
is called will give you a clear idea of how this class was designed to be extended.
For the sake of keeping the code clean, methods should be added at the block starting with the comment '//Top of Block behavioural methods'
*/
package com.WarwickWestonWright.HacerGesto;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public abstract class HGMove implements View.OnTouchListener, HacerGestoDown.IHacerGestoDown, HacerGestoMove.IHacerGestoMove, HacerGestoUp.IHacerGestoUp {

    private static boolean disableGestureInternally = false;
    private static boolean disableGestureDynamically = false;
    private static boolean suppressInvalidaton = false;
    private static boolean cumaulativeMove = true;
    private static float snapTolerances[] = null;
    private static Point[] snapPositions = null;
    private static int snapPositionsCount = 0;
    private static boolean snapFound = false;
    private static int snapFoundAtIdx = 0;
    private static boolean parentHasBeenRedrawn = false;

    private ImageView imageView = null;
    private HGResult hgResult = null;
    private Matrix matrix = null;
    private static int measureSpec = 0;

    private static boolean paddingIsExpanded = false;

    private static int firstPointerId = 0;
    private static int firstPointerIdx = 0;
    private static int secondPointerId = 0;
    private static int secondPointerIdx = 0;

    private static float[] matrixVals = null;
    private static int origonalWidth;
    private static int origonalHeight;
    private static int paddingL;
    private static int paddingT;
    private static int paddingR;
    private static int paddingB;

    private static float firstTouchX;
    private static float firstTouchY;
    private static float secondTouchX;
    private static float secondTouchY;
    private static float cumulativeMoveDistanceX;
    private static float cumulativeMoveDistanceY;
    private static float moveDistanceX;
    private static float moveDistanceY;

    private HacerGestoDown.IHacerGestoDown iHacerGestoDown = null;
    private HacerGestoMove.IHacerGestoMove iHacerGestoMove = null;
    private HacerGestoUp.IHacerGestoUp iHacerGestoUp = null;

    private static HacerGestoDown hacerGestoDown = null;
    private static HacerGestoMove hacerGestoMove = null;
    private static HacerGestoUp hacerGestoUp = null;

    public HGMove(ImageView imageView) {

        disableGestureInternally = false;
        disableGestureDynamically = false;
        suppressInvalidaton = false;
        cumaulativeMove = true;
        paddingIsExpanded = false;
        snapTolerances = null;
        snapPositionsCount = 0;
        snapFound = false;
        snapFoundAtIdx = 0;
        parentHasBeenRedrawn = false;

        firstPointerId = 0;
        firstPointerIdx = 0;
        secondPointerId = 0;
        secondPointerIdx = 0;

        matrixVals = new float[9];
        origonalWidth = 0;
        origonalHeight = 0;
        paddingL = imageView.getPaddingLeft();
        paddingT = imageView.getPaddingTop();
        paddingR = imageView.getPaddingRight();
        paddingB = imageView.getPaddingBottom();

        firstTouchX = 0;
        firstTouchY = 0;
        secondTouchX = 0;
        secondTouchY = 0;
        moveDistanceX = 0;
        moveDistanceY = 0;
        cumulativeMoveDistanceX = 0;
        cumulativeMoveDistanceY = 0;

        measureSpec = View.MeasureSpec.getSize(View.MeasureSpec.UNSPECIFIED);
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        imageView.measure(measureSpec, measureSpec);

        iHacerGestoDown = this;
        iHacerGestoMove = this;
        iHacerGestoUp = this;

        hacerGestoDown = new HacerGestoDown(imageView, HGFlags.MOVE) {};
        hacerGestoMove = new HacerGestoMove(imageView, HGFlags.MOVE) {};
        hacerGestoUp = new HacerGestoUp(imageView, HGFlags.MOVE) {};

        matrix = imageView.getImageMatrix();
        matrix.getValues(matrixVals);
        hgResult = new HGResult(matrixVals[Matrix.MSCALE_X], matrixVals[Matrix.MSCALE_Y], 0, 0, 0, 0, 0);
        hgResult.setWhatGesture(HGFlags.MOVE);
        origonalWidth = imageView.getMeasuredWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight();
        origonalHeight = imageView.getMeasuredHeight() - imageView.getPaddingTop() - imageView.getPaddingBottom();;
        this.imageView = imageView;

    }//End public HGMove(ImageView imageView)

    //Top of block standard event controllers down/move/up
    public void setFirstTouch(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return;

        }

        disableGestureInternally = false;
        firstPointerId = 0;
        firstPointerIdx = 0;
        secondPointerId = 0;
        secondPointerIdx = 0;

        try {

            try {

                if(event.getPointerCount() == 1) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);
                    secondTouchX = 0;
                    secondTouchY = 0;
                    moveDistanceX = 0;
                    moveDistanceY = 0;
                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);
                    hgResult.setSecondTouchX(0);
                    hgResult.setSecondTouchY(0);
                    hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                    hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                    if(cumaulativeMove == false) {

                        cumulativeMoveDistanceX = event.getX() - ((imageView.getMeasuredWidth() * matrixVals[Matrix.MSCALE_X]) / 2);
                        cumulativeMoveDistanceY = event.getY() - ((imageView.getMeasuredHeight() * matrixVals[Matrix.MSCALE_Y]) / 2);

                    }

                }//End if(event.getPointerCount() == 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setFirstTouch(MotionEvent event)


    public void setSecondTouch(MotionEvent event) {

        if (disableGestureDynamically == true) {

            return;

        }

        disableGestureInternally = true;

        try {

            try {

                secondPointerId = event.getPointerId(1);
                secondPointerIdx = event.findPointerIndex(secondPointerId);
                secondTouchX = event.getX(secondPointerIdx);
                secondTouchY = event.getY(secondPointerIdx);
                hgResult.setSecondTouchX(secondTouchX);
                hgResult.setSecondTouchY(secondTouchY);
                hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

    }//End public void setSecondTouch(MotionEvent event)


    public HGResult getDown(MotionEvent event) {

        return hgResult;

    }


    public HGResult getMotion(MotionEvent event) {

        //Dev Note: this if block is an unclean solution. Problem was when imageView is contained in it's own layout it wasn't working on older devices. Needs fixing (not priority.)
        if(imageView.isFocused() == false) {

            (imageView.getParent()).requestLayout();

        }

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(disableGestureInternally == true) {

            return hgResult;

        }

        //Top of block to call extendible behaviour methods that may be added to this class
        if(snapPositionsCount > 0) {

            moveSnapMotion(event);
            return hgResult;

        }
        //Bottom of block to call extendible behaviour methods that may be added to this class

        try {

            try {

                if(event.getPointerCount() > 1) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    secondPointerId = event.getPointerId(1);
                    secondPointerIdx = event.findPointerIndex(secondPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);
                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);
                    secondTouchX = event.getX(secondPointerIdx);
                    secondTouchY = event.getY(secondPointerIdx);
                    hgResult.setSecondTouchX(secondTouchX);
                    hgResult.setSecondTouchY(secondTouchY);

                    return hgResult;

                }
                else {

                    hgResult.setFirstTouchX(event.getX());
                    hgResult.setFirstTouchY(event.getY());

                    matrix = imageView.getImageMatrix();
                    matrix.getValues(matrixVals);

                    matrixVals[Matrix.MTRANS_X] -= (matrixVals[Matrix.MTRANS_X] - event.getX()) + firstTouchX - cumulativeMoveDistanceX;
                    matrixVals[Matrix.MTRANS_Y] -= (matrixVals[Matrix.MTRANS_Y] - event.getY()) + firstTouchY - cumulativeMoveDistanceY;

                    if(suppressInvalidaton == false) {

                        matrix.setValues(matrixVals);
                        imageView.setImageMatrix(matrix);
                        imageView.invalidate();

                    }

                }//End if(event.getPointerCount() > 1)

                hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

        return hgResult;

    }//End public HGResult getMotion(MotionEvent event)


    public HGResult getUp(MotionEvent event) {

        if(disableGestureDynamically == true) {

            return hgResult;

        }

        if(disableGestureInternally == true) {

            return hgResult;

        }

        if(event.getPointerCount() > 1) {

            firstPointerId = event.getPointerId(0);
            firstPointerIdx = event.findPointerIndex(firstPointerId);
            secondPointerId = event.getPointerId(1);
            secondPointerIdx = event.findPointerIndex(secondPointerId);

            secondTouchX = event.getX(secondPointerIdx);
            secondTouchY = event.getY(secondPointerIdx);
            hgResult.setSecondTouchX(secondTouchX);
            hgResult.setSecondTouchY(secondTouchY);

            moveDistanceX = firstTouchX - event.getX(event.getActionIndex());
            moveDistanceY = firstTouchY - event.getY(event.getActionIndex());

            if((moveDistanceX == 0) && (moveDistanceY == 0)) {

                firstTouchX = event.getX(secondPointerIdx);
                firstTouchY = event.getY(secondPointerIdx);

            }
            else {

                firstTouchX = event.getX(firstPointerIdx);
                firstTouchY = event.getY(firstPointerIdx);

            }

            return hgResult;

        }
        else /*if(event.getPointerCount() == 1)*/ {

            matrixVals[Matrix.MTRANS_X] -= (matrixVals[Matrix.MTRANS_X] - event.getX()) + firstTouchX - cumulativeMoveDistanceX;
            matrixVals[Matrix.MTRANS_Y] -= (matrixVals[Matrix.MTRANS_Y] - event.getY()) + firstTouchY - cumulativeMoveDistanceY;

            if(cumaulativeMove == true) {

                //Move cumulatively
                cumulativeMoveDistanceX -= (firstTouchX - event.getX(firstPointerIdx));
                cumulativeMoveDistanceY -= (firstTouchY - event.getY(firstPointerIdx));

            }

            if(snapFound == false) {

                matrix.setValues(matrixVals);
                imageView.setImageMatrix(matrix);
                imageView.invalidate();

            }

            if(snapPositionsCount > 0) {snapFound = false;}

            firstTouchX = event.getX();
            firstTouchY = event.getY();
            hgResult.setFirstTouchX(firstTouchX);
            hgResult.setFirstTouchY(firstTouchY);

            hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
            hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

            return hgResult;

        }//End if(event.getPointerCount() > 1)

    }//End public HGResult getUp(MotionEvent event)


    //Top of Block behavioural methods
    public void moveSnapMotion(MotionEvent event) {

        try {

            try {

                if(event.getPointerCount() > 1) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    secondPointerId = event.getPointerId(1);
                    secondPointerIdx = event.findPointerIndex(secondPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);
                    hgResult.setFirstTouchX(firstTouchX);
                    hgResult.setFirstTouchY(firstTouchY);
                    secondTouchX = event.getX(secondPointerIdx);
                    secondTouchY = event.getY(secondPointerIdx);
                    hgResult.setSecondTouchX(secondTouchX);
                    hgResult.setSecondTouchY(secondTouchY);

                    return;

                }
                else {

                    hgResult.setFirstTouchX(event.getX());
                    hgResult.setFirstTouchY(event.getY());

                    for(int i = 0; i < snapPositionsCount; i++) {

                        if(!(Math.abs(matrixVals[Matrix.MTRANS_X] - snapPositions[i].x) < snapTolerances[i]) || !(Math.abs(matrixVals[Matrix.MTRANS_Y] - snapPositions[i].y) < snapTolerances[i])) {

                            if(snapFound == false) {

                                matrixVals[Matrix.MTRANS_X] -= (matrixVals[Matrix.MTRANS_X] - event.getX()) + firstTouchX - cumulativeMoveDistanceX;
                                matrixVals[Matrix.MTRANS_Y] -= (matrixVals[Matrix.MTRANS_Y] - event.getY()) + firstTouchY - cumulativeMoveDistanceY;

                                hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                                hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                                if(suppressInvalidaton == false) {

                                    matrix.setValues(matrixVals);
                                    imageView.setImageMatrix(matrix);
                                    imageView.invalidate();

                                }

                            }//End if(snapFound == false)

                        }
                        else {

                            matrixVals[Matrix.MTRANS_X] = snapPositions[i].x;
                            matrixVals[Matrix.MTRANS_Y] = snapPositions[i].y;

                            hgResult.setXPos(matrixVals[Matrix.MTRANS_X]);
                            hgResult.setYPos(matrixVals[Matrix.MTRANS_Y]);

                            if(suppressInvalidaton == false) {

                                matrix.setValues(matrixVals);
                                imageView.setImageMatrix(matrix);
                                imageView.invalidate();

                            }

                            if(snapFound == false) {

                                snapFoundAtIdx = i;

                            }

                            snapFound = true;

                            if((Math.abs(((event.getX() - firstTouchX) + cumulativeMoveDistanceX) - snapPositions[snapFoundAtIdx].x) > snapTolerances[snapFoundAtIdx]) ||
                                    (Math.abs(((event.getY() - firstTouchY) + cumulativeMoveDistanceY) - snapPositions[snapFoundAtIdx].y) > snapTolerances[snapFoundAtIdx])) {

                                matrixVals[Matrix.MTRANS_X] -= (matrixVals[Matrix.MTRANS_X] - event.getX()) + firstTouchX - cumulativeMoveDistanceX;
                                matrixVals[Matrix.MTRANS_Y] -= (matrixVals[Matrix.MTRANS_Y] - event.getY()) + firstTouchY - cumulativeMoveDistanceY;

                                snapFound = false;

                            }//End if ....

                        }//End if(!(Math.abs(matrixVals[Matrix.MTRANS_X] - snapPositions[i].x) < snapTolerances[i]) || !(Math.abs(matrixVals[Matrix.MTRANS_Y] - snapPositions[i].y) < snapTolerances[i]))

                    }//End for(int i = 0; i < positionsCount; i++)

                }//End if(event.getPointerCount() > 1)

            }
            catch(IndexOutOfBoundsException e) {

                Log.wtf("IndexOutOfBoundsException", e.getMessage());

            }

        }
        catch(IllegalArgumentException e) {

            Log.wtf("IllegalArgumentException", e.getMessage());

        }

        return;

    }//End public void moveSnapMotion(MotionEvent event)


    public void setMoveSnap(Point[] snapPositions, float snapTolerances[]) {

        this.snapTolerances = snapTolerances;
        this.snapPositions = snapPositions;

        if(snapPositions != null && snapTolerances != null) {

            snapPositionsCount = snapPositions.length;

        }
        else {

            snapPositionsCount = 0;

        }

    }//End public void setMoveSnap(Point[] snapPositions, float snapTolerances[])


    public float[] getMoveSnapTolerance() {

        return snapTolerances;

    }


    public Point[] getMoveSnapPositions() {

        return snapPositions;

    }

    public void setCumaulativeMove(boolean cumaulativeMove) {

        this.cumaulativeMove = cumaulativeMove;

    }


    public boolean getCumaulativeMove() {

        return cumaulativeMove;

    }
    //Bottom of Block behavioural methods


    //Top of Block convenience methods
    public void manualMove(float xPos, float yPos) {

        matrix = imageView.getImageMatrix();
        matrix.getValues(matrixVals);
        matrixVals[Matrix.MTRANS_X] = xPos;
        matrixVals[Matrix.MTRANS_Y] = yPos;
        matrix.setValues(matrixVals);
        imageView.setImageMatrix(matrix);
        imageView.invalidate();
        hgResult.setXPos(xPos);
        hgResult.setYPos(yPos);

        if(cumaulativeMove == true) {

            cumulativeMoveDistanceX += xPos;
            cumulativeMoveDistanceY += yPos;

        }

    }//End public void manualMove(float xPos, float yPos)
    //Bottom of Block convenience methods


    //Top of Block Common Functions
    public Bitmap getBitmapFromImageView(ImageView imageView) {

        return ((BitmapDrawable) imageView.getDrawable()).getBitmap();

    }


    public void suppressInvalidaton(boolean suppressInvalidaton) {

        this.suppressInvalidaton = suppressInvalidaton;

    }


    public boolean getSuppressInvalidaton() {

        return suppressInvalidaton;

    }


    public void setDisableGesture(boolean disableGestureDynamically) {

        this.disableGestureDynamically = disableGestureDynamically;

    }


    public boolean isGesturedisabled() {

        return disableGestureDynamically;

    }


    public float[] getMatrixVals() {

        matrix.getValues(matrixVals);
        return matrixVals;

    }


    public void setMatrixVals(float[] matrixVals) {

        matrix = imageView.getImageMatrix();
        matrix.getValues(this.matrixVals);
        matrix.setValues(this.matrixVals);
        imageView.invalidate();

        cumulativeMoveDistanceX = this.matrixVals[Matrix.MTRANS_X] - ((this.matrixVals[Matrix.MTRANS_X] - this.matrixVals[Matrix.MTRANS_X]) / this.matrixVals[Matrix.MSCALE_X]);
        cumulativeMoveDistanceY = this.matrixVals[Matrix.MTRANS_Y] - ((this.matrixVals[Matrix.MTRANS_Y] - this.matrixVals[Matrix.MTRANS_Y]) / this.matrixVals[Matrix.MSCALE_Y]);

        if(Float.isNaN(cumulativeMoveDistanceX)) {

            cumulativeMoveDistanceX = this.matrixVals[Matrix.MTRANS_X] - ((this.matrixVals[Matrix.MTRANS_X] - this.matrixVals[Matrix.MTRANS_X]));

        }

        if(Float.isNaN(cumulativeMoveDistanceY)) {

            cumulativeMoveDistanceY = this.matrixVals[Matrix.MTRANS_Y] - ((this.matrixVals[Matrix.MTRANS_Y] - this.matrixVals[Matrix.MTRANS_Y]));

        }

    }//End public void setMatrixVals(float[] matrixVals)


    public void onDown(HGResult hgResult) {

        hacerGestoDown = null;

    }


    public void onMotion(HGResult hgResult) {

        hacerGestoMove = null;

    }


    public void onRelease(HGResult hgResult) {

        hacerGestoUp = null;

    }


    public HGResult getHgResult() {

        return this.hgResult;

    }


    public void expandPaddingToFitContainer(final boolean expandPadding) {

        paddingIsExpanded = expandPadding;

        origonalWidth = imageView.getMeasuredWidth();
        origonalHeight = imageView.getMeasuredHeight();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {

                                if (expandPadding == true) {

                                    paddingL = imageView.getLeft();
                                    paddingT = imageView.getTop();
                                    paddingR = ((ViewGroup) imageView.getParent()).getMeasuredWidth() - imageView.getRight();
                                    paddingB = ((ViewGroup) imageView.getParent()).getMeasuredHeight() - imageView.getBottom();
                                    imageView.setPadding(paddingL, paddingT, paddingR, paddingB);

                                }
                                else {

                                    paddingL = imageView.getPaddingLeft();
                                    paddingT = imageView.getPaddingTop();
                                    paddingR = imageView.getPaddingRight();
                                    paddingB = imageView.getPaddingBottom();
                                    imageView.setPadding(0, 0, 0, 0);

                                }//End if(expandPadding == true)

                                (imageView.getParent()).requestLayout();

                            }

                        }, 10);

            }

        });

    }//End public void expandPaddingToFitContainer(final boolean expandPadding)
    //Bottom of Block Common Functions


    @Override
    public boolean onTouch(View view, MotionEvent event) {

        final int action = event.getAction() & MotionEvent.ACTION_MASK;

        switch(action) {

            case MotionEvent.ACTION_DOWN: {

                if(hacerGestoDown != null) {

                    setFirstTouch(event);
                    iHacerGestoDown.onDown(getDown(event));

                }
                else {

                    setFirstTouch(event);

                }

                break;

            }
            case MotionEvent.ACTION_POINTER_DOWN: {

                if(hacerGestoDown != null) {

                    setSecondTouch(event);
                    iHacerGestoDown.onDown(getDown(event));

                }
                else {

                    setSecondTouch(event);

                }

                break;

            }
            case MotionEvent.ACTION_MOVE: {

                if(hacerGestoMove != null) {

                    iHacerGestoMove.onMotion(getMotion(event));

                }

                break;

            }
            case MotionEvent.ACTION_POINTER_UP: {

                if(hacerGestoUp != null) {

                    iHacerGestoUp.onRelease(getUp(event));

                }
                else {

                    getUp(event);

                }

                break;

            }
            case MotionEvent.ACTION_UP: {

                if(hacerGestoUp != null) {

                    iHacerGestoUp.onRelease(getUp(event));

                }
                else {

                    getUp(event);

                }

                break;

            }
            default:

                break;

        }//End switch(action)

        return true;

    }//End public boolean onTouch(View view, MotionEvent event)

}