package com.WarwickWestonWright.HacerGesto;

public class HGFlags {

    public static final int NO_GESTURE = 0;
    public static final int MOVE = 1;
    public static final int FLING = 2;
    public static final int SCALE = 4;
    public static final int ROTATE = 8;

}